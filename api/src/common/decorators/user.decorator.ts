import { createParamDecorator } from '@nestjs/common';

// tslint:disable-next-line: variable-name
export const UserDecorator = createParamDecorator(
    (_, req) => req.user,
    );
