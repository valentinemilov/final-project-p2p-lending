import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { P2PLendingSystemError } from '../exceptions/p2p-lending-system.error';

@Catch(P2PLendingSystemError)
export class P2PLendingSystemErrorFilter implements ExceptionFilter {
    public catch(exception: P2PLendingSystemError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();

        response.status(exception.code).json({
            status: exception.code,
            error: exception.message,
        });
    }
}
