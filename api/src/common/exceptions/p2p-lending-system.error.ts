export class P2PLendingSystemError extends Error {
    public constructor(message?: string, public code?: number) {
      super(message);
    }
  }
