import { UserRole } from '../enums/user-role.enum';

export interface JwtPayload {
    id: string;
    username: string;
    role: UserRole;
}
