import { ApiModelProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsNumber, Min } from 'class-validator';

export class CreateInvestmentProposalDTO {

    @ApiModelProperty({ default: 20000 })
    @IsNumber()
    @Min(1)
    @IsNotEmpty()
    proposed_amount: number;

    @ApiModelProperty({ default: 12 })
    @IsNumber()
    @Min(1)
    @IsNotEmpty()
    proposed_period_months: number;

    @ApiModelProperty({ default: 0.032 })
    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    proposed_interest: number;

    @ApiModelProperty({ default: 0.01 })
    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    penalty_rate: number;

    @ApiModelProperty({ example: '2019-08-19' })
    @IsString()
    creation_date: string;
}
