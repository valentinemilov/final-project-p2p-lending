import { Publish } from '../../transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';
import { InvesmentProposalStatus } from '../../common/enums/investment-proposal-status.enum';
import { User } from '../../database/entities/user.entity';
import { DebtRequest } from '../../database/entities/debt-request.entity';
import { UserDTO } from '../../users/models/user.dto';
import { OpenDebtRequestDTO } from '../../debt-request/models/open-debt-requests.dto';

export class InvestmentProposalDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty({ example: '20000' })
    @Publish()
    proposed_amount: number;

    @ApiModelProperty({ example: '12' })
    @Publish()
    proposed_period_months: number;

    @ApiModelProperty({ example: '3.25' })
    @Publish()
    proposed_interest: number;

    @ApiModelProperty({ example: '1.25' })
    @Publish()
    penalty_rate: number;

    @ApiModelProperty({ example: 'Pending' })
    @Publish()
    proposal_status: InvesmentProposalStatus;

    @ApiModelProperty()
    @Publish(UserDTO)
    user: User;

    @ApiModelProperty()
    @Publish(OpenDebtRequestDTO)
    debt: DebtRequest;
}
