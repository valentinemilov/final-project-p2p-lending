import { IsNotEmpty, IsOptional, IsNumber, Min, IsString } from 'class-validator';

export class UpdateInvestmentProposalDTO {

    @IsString()
    @IsNotEmpty()
    id: string;

    @IsOptional()
    @IsNumber()
    @Min(0)
    proposed_interest: number;

    @IsOptional()
    @IsNumber()
    @Min(0)
    penalty_rate: number;
}
