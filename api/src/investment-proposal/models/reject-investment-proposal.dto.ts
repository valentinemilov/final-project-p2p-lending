import { InvesmentProposalStatus } from '../../common/enums/investment-proposal-status.enum';
import { Publish } from '../../transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';

export class RejectInvestmentProposalDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty({ example: '20000' })
    @Publish()
    proposed_amount: number;

    @ApiModelProperty({ example: '12' })
    @Publish()
    proposed_period_months: number;

    @ApiModelProperty({ example: '3.25' })
    @Publish()
    proposed_interest: number;

    @ApiModelProperty({ example: 'Pending' })
    @Publish()
    proposal_status: InvesmentProposalStatus;

    @Publish()
    penalty_rate: number;
}
