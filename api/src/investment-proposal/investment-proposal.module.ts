import { Module } from '@nestjs/common';
import { InvestmentProposalController } from './investment-proposal.controller';
import { InvestmentProposalService } from './investment-proposal.service';
import { DebtRequest } from '../database/entities/debt-request.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { InvestmentProposal } from '../database/entities/investment-proposal.entity';

@Module({
    imports: [TypeOrmModule.forFeature([DebtRequest, User, InvestmentProposal])],
    controllers: [InvestmentProposalController],
    providers: [InvestmentProposalService],
})
export class InvestmentProposalModule { }
