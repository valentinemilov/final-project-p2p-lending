import {
  Controller,
  Get,
  UseGuards,
  Post,
  Body,
  Param,
  UseInterceptors,
  HttpCode,
  HttpStatus,
  Delete,
  Put,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';

import { InvestmentProposal } from '../database/entities/investment-proposal.entity';
import { ApiBearerAuth, ApiUseTags } from '@nestjs/swagger';
import { InvestmentProposalService } from './investment-proposal.service';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { CreateInvestmentProposalDTO } from './models/create-investment-proposal.dto';
import { InvestmentProposalDTO } from './models/investment-proposal.dto';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { RejectInvestmentProposalDTO } from './models/reject-investment-proposal.dto';
import { OwnInvestmentProposalsDTO } from './models/own-investment-proposals.dto';
import { UpdateInvestmentProposalDTO } from './models/update-investment-proposal.dto';

@ApiUseTags('Investment-Proposal Controller')
@ApiBearerAuth()
@UseGuards(AuthGuardWithBlacklisting)
@Controller()
export class InvestmentProposalController {

  constructor(
    private readonly investmentProposalService: InvestmentProposalService,
  ) { }

  @Get('investor/investmentproposal')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(OwnInvestmentProposalsDTO))
  async all(
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<InvestmentProposal[]> {

    return await this.investmentProposalService.getAll(user);
  }

  @Post('debtrequest/:debtrequestId/investmentproposal')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(InvestmentProposalDTO))
  async create(
    @Body() investmentProposal: CreateInvestmentProposalDTO,
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('debtrequestId') debtId: string,
  ): Promise<InvestmentProposal> {

    return await this.investmentProposalService.create(investmentProposal, debtId, user);
  }

  @Delete('user/investmentproposal/:investmentproposalId')
  @HttpCode(HttpStatus.OK)
  async delete(
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('investmentproposalId') investmentproposalId: string,
  ): Promise<InvestmentProposal> {
    const deletedProposal = await this.investmentProposalService.delete(investmentproposalId, user);

    return deletedProposal;
  }

  @Put('debtrequest/:debtrequestId/investmentproposal')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(InvestmentProposalDTO))
  async update(
    @Body() investmentProposal: UpdateInvestmentProposalDTO,
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('debtrequestId') debtId: string,
  ): Promise<InvestmentProposal> {

    return await this.investmentProposalService.update(investmentProposal, debtId, user);
  }

  @Put('debtrequest/:debtrequestId/investmentproposal/:investmentproposalId')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(RejectInvestmentProposalDTO))
  async rejectProposal(
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('debtrequestId') requestId: string,
    @Param('investmentproposalId') proposalId: string,
  ): Promise<InvestmentProposal> {

    return await this.investmentProposalService.rejectProposal(requestId, proposalId, user);
  }
}
