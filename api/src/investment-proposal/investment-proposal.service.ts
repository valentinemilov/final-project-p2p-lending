import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InvestmentProposal } from '../database/entities/investment-proposal.entity';
import { Repository } from 'typeorm';
import { DebtRequest } from '../database/entities/debt-request.entity';
import { CreateInvestmentProposalDTO } from './models/create-investment-proposal.dto';
import { User } from '../database/entities/user.entity';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { UserDTO } from '../users/models/user.dto';
import { InvesmentProposalStatus } from '../common/enums/investment-proposal-status.enum';
import { UpdateInvestmentProposalDTO } from './models/update-investment-proposal.dto';

@Injectable()
export class InvestmentProposalService {

    constructor(
        @InjectRepository(InvestmentProposal) private readonly investmentProposalRepository: Repository<InvestmentProposal>,
        @InjectRepository(DebtRequest) private readonly debtRequestRepository: Repository<DebtRequest>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async getAll(user: UserDTO): Promise<InvestmentProposal[]> {
        const foundUser: User = await this.findUserById(user.id);
        const allProposals = await this.investmentProposalRepository.find({
            where: {
                user: foundUser,
                isDeleted: false,
                proposal_status: InvesmentProposalStatus.Pending,
            },
            relations: ['user', 'debt', 'debt.user'],
        });

        return allProposals;
    }

    async create(investmentProposal: CreateInvestmentProposalDTO, debtId: string, user: UserDebtRequestDTO): Promise<InvestmentProposal> {

        const newInvestmentProposal: InvestmentProposal = this.investmentProposalRepository.create(investmentProposal);
        const foundUser: User = await this.findUserById(user.id);
        const foundDebtRequest: DebtRequest = await this.debtRequestRepository.findOne({
            where: {
                id: debtId, isDeleted: false,
            },
        });

        newInvestmentProposal.debt = Promise.resolve(foundDebtRequest);
        newInvestmentProposal.user = Promise.resolve(foundUser);
        const savedInvestmentProposal = await this.investmentProposalRepository.save(newInvestmentProposal);

        return savedInvestmentProposal;
    }

    async delete(investmentProposalID: string, user: UserDebtRequestDTO): Promise<InvestmentProposal> {
        const foundUser = await this.findUserById(user.id);
        const foundInvestmentProposal: InvestmentProposal = await this.findInvestmentProposalById(investmentProposalID);

        const userInFoundProposal = await foundInvestmentProposal.user;
        if (foundUser.id !== userInFoundProposal.id) {
            throw new P2PLendingSystemError('Investment proposal does not belong to the user', 400);
        }
        foundInvestmentProposal.isDeleted = true;
        const savedReview = await this.investmentProposalRepository.save(foundInvestmentProposal);

        return savedReview;
    }

    async update(investmentProposal: UpdateInvestmentProposalDTO, debtId: string, user: UserDebtRequestDTO): Promise<InvestmentProposal> {

        const foundUser: User = await this.findUserById(user.id);
        const foundInvestmentProposal: InvestmentProposal = await this.findInvestmentProposalById(investmentProposal.id);
        const userInFoundProposal: User = await foundInvestmentProposal.user;
        if (foundUser.id !== userInFoundProposal.id) {
            throw new P2PLendingSystemError('Investment proposal does not belong to the user', 400);
        }

        const updatedInformation = {
            proposed_interest: investmentProposal.proposed_interest,
            penalty_rate: investmentProposal.penalty_rate,
        };

        const updatedInvestmentProposal: InvestmentProposal = { ...foundInvestmentProposal, ...updatedInformation };
        const savedInvestmentProposal = await this.investmentProposalRepository.save(updatedInvestmentProposal);

        return savedInvestmentProposal;
    }

    private async findUserById(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('The user is not found', 400);
        }

        return foundUser;
    }

    private async findInvestmentProposalById(investmentProposalId: string) {
        const foundInvestmentProposal: InvestmentProposal = await this.investmentProposalRepository.findOne({
            where: {
                id: investmentProposalId,
                isDeleted: false,
            },
            relations: ['user', 'debt'],
        });

        if (!foundInvestmentProposal) {
            throw new P2PLendingSystemError('The investment proposal is not found', 400);
        }

        return foundInvestmentProposal;
    }

    private async findDebtRequestById(debtRequestId: string) {
        const foundDebtRequest: DebtRequest = await this.debtRequestRepository.findOne({
            where: {
                id: debtRequestId,
                isDeleted: false,
            },
            relations: ['user'],
        });

        if (!foundDebtRequest) {
            throw new P2PLendingSystemError('The debt request is not found', 400);
        }

        return foundDebtRequest;
    }

    public async rejectProposal(requestId: string, proposalId: string, user: UserDebtRequestDTO): Promise<InvestmentProposal> {
        const foundUser: User = await this.findUserById(user.id);
        const foundDebtRequest: DebtRequest = await this.findDebtRequestById(requestId);
        const foundInvestmentProposal: InvestmentProposal = await this.findInvestmentProposalById(proposalId);
        const userInFoundDebtRequest = await foundDebtRequest.user;

        if (userInFoundDebtRequest.id !== foundUser.id) {
            throw new P2PLendingSystemError('Debt request does not belong to the user', 400);
        }

        if (foundInvestmentProposal.proposal_status === 1) {
            foundInvestmentProposal.proposal_status = InvesmentProposalStatus.Rejected;
        }

        await this.investmentProposalRepository.save(foundInvestmentProposal);

        return foundInvestmentProposal;
    }

}
