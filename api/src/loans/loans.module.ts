import { InvestmentProposal } from './../database/entities/investment-proposal.entity';
import { Module } from '@nestjs/common';
import { LoansService } from './loans.service';
import { LoansController } from './loans.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Loan } from '../database/entities/loan.entity';
import { User } from '../database/entities/user.entity';
import { LoanPayments } from '../database/entities/loan-payments.entity';
import { DebtRequest } from '../database/entities/debt-request.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Loan, User, LoanPayments, DebtRequest, InvestmentProposal])],
  providers: [LoansService],
  controllers: [LoansController],
})
export class LoansModule { }
