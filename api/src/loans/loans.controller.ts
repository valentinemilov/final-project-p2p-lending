import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  UseGuards,
  Param,
  Post,
} from '@nestjs/common';

import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { LoansService } from './loans.service';
import { Loan } from '../database/entities/loan.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { LoanDTO } from './models/loan.dto';

@ApiUseTags('Loan Controller')
@ApiBearerAuth()
@UseGuards(AuthGuardWithBlacklisting)
@Controller()
export class LoansController {
  constructor(
    private readonly loansService: LoansService,
  ) { }

  @Get('loans/investor')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  async allInvestorLoans(
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<Loan[]> {
    const loans = await this.loansService.allInvestorLoans(user);

    return loans;
  }

  @Get('loans/borrower')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  async allBorrowerLoans(
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<Loan[]> {
    const loans = await this.loansService.allBorrowerLoans(user);

    return loans;
  }

  @Post('debtrequest/:debtrequestId/investmentproposal/:investmentproposalId/loans')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(new TransformInterceptor(LoanDTO))
  public async createLoan(
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('debtrequestId') requestId: string,
    @Param('investmentproposalId') proposalId: string,
  ): Promise<Loan> {

    return await this.loansService.createNewLoan(requestId, proposalId, user);
  }
}
