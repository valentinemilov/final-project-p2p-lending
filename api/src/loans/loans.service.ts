import { UserDebtRequestDTO } from './../debt-request/models/user-debt-request.dto';
import { Injectable } from '@nestjs/common';
import { Loan } from '../database/entities/loan.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../database/entities/user.entity';
import { InvestmentProposal } from '../database/entities/investment-proposal.entity';
import { UserDTO } from '../users/models/user.dto';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';
import { DebtRequest } from '../database/entities/debt-request.entity';
import moment from 'moment';
import { InvesmentProposalStatus } from '../common/enums/investment-proposal-status.enum';

@Injectable()
export class LoansService {
    constructor(
        @InjectRepository(Loan) private readonly loansRepository: Repository<Loan>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(DebtRequest) private readonly debtRequestRepository: Repository<DebtRequest>,
        @InjectRepository(InvestmentProposal) private readonly investmentProposalRepository: Repository<InvestmentProposal>,
    ) { }

    async allInvestorLoans(user: UserDTO): Promise<Loan[]> {
        const allLoans = await this.loansRepository.find({
            where: {
                investor: user.id,
                isDeleted: false,
                isClosed: false,
            },
            relations: ['payments', 'borrower', 'investor'],
        });

        return allLoans;
    }

    async allBorrowerLoans(user: UserDTO): Promise<Loan[]> {
        const allLoans = await this.loansRepository.find({
            where: {
                borrower: user.id,
                isDeleted: false,
                isClosed: false,
            },
            relations: ['payments', 'borrower', 'investor'],
        });

        return allLoans;
    }

    public async createNewLoan(requestId: string, proposalId: string, user: UserDebtRequestDTO): Promise<Loan> {
        const loanEntity: Loan = this.loansRepository.create();
        const foundBorrower: User = await this.findUserById(user.id);
        const foundDebtRequest: DebtRequest = await this.findDebtRequestById(requestId);
        const foundInvestmentProposal: InvestmentProposal = await this.findInvestmentProposalById(proposalId);
        const foundInvestor = await foundInvestmentProposal.user;
        const investortoUpdate: User = await this.findUserById(foundInvestor.id);

        foundInvestmentProposal.proposal_status = InvesmentProposalStatus.Accepted;
        foundDebtRequest.isClosed = true;

        const investmentProposalsInDebtRequest: InvestmentProposal[] = await foundDebtRequest.investment_proposals;
        const rejectedInvestmentProposals = investmentProposalsInDebtRequest
            .filter((curr: InvestmentProposal) => curr.id !== foundInvestmentProposal.id)
            .map(async (prop: InvestmentProposal) => {
                prop.proposal_status = InvesmentProposalStatus.Rejected;
                await this.investmentProposalRepository.save(prop);
            });

        loanEntity.amount = foundDebtRequest.target_amount;
        loanEntity.period_months = foundDebtRequest.target_period_months;
        loanEntity.debt_request = Promise.resolve(foundDebtRequest);

        loanEntity.annual_interest = foundInvestmentProposal.proposed_interest;
        loanEntity.penalty_rate = foundInvestmentProposal.penalty_rate;
        loanEntity.investment_proposals = Promise.resolve([foundInvestmentProposal]);
        loanEntity.creation_date = moment(new Date()).format('YYYY-MM-DD');
        loanEntity.borrower = Promise.resolve(foundBorrower);

        const investor = await foundInvestmentProposal.user;
        loanEntity.investor = Promise.resolve(investor);

        foundBorrower.balance += loanEntity.amount;
        const savedUpdatedBorrower = await this.usersRepository.save(foundBorrower);

        investortoUpdate.balance -= loanEntity.amount;
        const savedUpdatedInvestor = await this.usersRepository.save(investortoUpdate);

        const savedInvestmentProposal = await this.investmentProposalRepository.save(foundInvestmentProposal);
        const savedDebtRequest = await this.debtRequestRepository.save(foundDebtRequest);
        const savedLoan = await this.loansRepository.save(loanEntity);

        return savedLoan;
    }

    private async findUserById(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('The user is not found', 400);
        }

        return foundUser;
    }

    private async findDebtRequestById(debtRequestId: string) {
        const foundDebtRequest: DebtRequest = await this.debtRequestRepository.findOne({
            where: {
                id: debtRequestId,
                isDeleted: false,
                isClosed: false,
            },
        });

        if (!foundDebtRequest) {
            throw new P2PLendingSystemError('The debt request is not found', 400);
        }

        return foundDebtRequest;
    }

    private async findInvestmentProposalById(investmentProposalId: string) {
        const foundInvestmentProposal: InvestmentProposal = await this.investmentProposalRepository.findOne({
            where: {
                id: investmentProposalId,
                isDeleted: false,
                proposal_status: InvesmentProposalStatus.Pending,
            },
            relations: ['user'],
        });

        if (!foundInvestmentProposal) {
            throw new P2PLendingSystemError('The investment proposal is not found', 400);
        }

        return foundInvestmentProposal;
    }
}
