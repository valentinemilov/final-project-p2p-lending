import { UserDTO } from '../../users/models/user.dto';
import { LoanPaymentsDTO } from './loan-payments.dto';
import { Publish } from '../../transformer/decorators/publish';
import { User } from '../../database/entities/user.entity';
import { Loan } from '../../database/entities/loan.entity';

export class LoanDTO {

    @Publish()
    id: string;

    @Publish()
    amount: number;

    @Publish()
    period_months: number;

    @Publish()
    annual_interest: number;

    @Publish()
    creation_date: string;

    @Publish()
    penalty_rate: number;

    @Publish()
    isClosed: boolean;

    @Publish()
    isDeleted: boolean;

    @Publish(UserDTO)
    investor: User;

    @Publish(UserDTO)
    borrower: User;

    @Publish(LoanPaymentsDTO)
    payments: Loan;
}
