import { Publish } from '../../transformer/decorators/publish';

export class LoanPaymentsDTO {

    @Publish()
    id: string;

    @Publish()
    amount_paid: number;

    @Publish()
    interest_paid: number;

    @Publish()
    overdue_paid: number;

    @Publish()
    payment_date: string;
}
