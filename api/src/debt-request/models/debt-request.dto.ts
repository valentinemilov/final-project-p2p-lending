import { Publish } from '../../transformer/decorators/publish';
import { UserDebtRequestDTO } from './user-debt-request.dto';
import { User } from '../../database/entities/user.entity';
import { InvestDebtRequestDTO } from './invest-debt-request.dto';
import { InvestmentProposal } from '../../database/entities/investment-proposal.entity';
import { ApiModelProperty } from '@nestjs/swagger';

export class DebtRequestDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty()
    @Publish()
    target_amount: number;

    @ApiModelProperty()
    @Publish()
    target_period_months: number;

    @ApiModelProperty()
    @Publish()
    date: string;

    @ApiModelProperty()
    @Publish(UserDebtRequestDTO)
    user: User;

    @ApiModelProperty()
    @Publish(InvestDebtRequestDTO)
    investment_proposals: InvestmentProposal[];
}
