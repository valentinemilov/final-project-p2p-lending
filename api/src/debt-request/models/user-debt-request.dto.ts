import { Publish } from '../../transformer/decorators/publish';
import { ApiModelProperty } from '@nestjs/swagger';

export class UserDebtRequestDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty()
    @Publish()
    username: string;

    @ApiModelProperty()
    @Publish()
    email: string;

    @ApiModelProperty()
    @Publish()
    balance: number;
}
