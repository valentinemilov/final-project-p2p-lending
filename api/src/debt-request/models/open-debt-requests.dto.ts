import { ApiModelProperty } from '@nestjs/swagger';
import { Publish } from '../../transformer/decorators/publish';
import { User } from '../../database/entities/user.entity';
import { UserDebtRequestDTO } from './user-debt-request.dto';

export class OpenDebtRequestDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty()
    @Publish()
    target_amount: number;

    @ApiModelProperty()
    @Publish()
    target_period_months: number;

    @ApiModelProperty()
    @Publish()
    date: string;

    @ApiModelProperty()
    @Publish(UserDebtRequestDTO)
    user: User;
}
