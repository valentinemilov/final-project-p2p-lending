import { IsNumber, IsOptional, Min } from 'class-validator';

export class UpdateDebtRequestDTO {

    @IsOptional()
    @IsNumber()
    @Min(1)
    target_amount: number;

    @IsOptional()
    @IsNumber()
    @Min(1)
    target_period_months: number;
}
