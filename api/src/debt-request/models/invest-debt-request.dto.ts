import { Publish } from '../../transformer/decorators/publish';
import { InvesmentProposalStatus } from '../../common/enums/investment-proposal-status.enum';
import { ApiModelProperty } from '@nestjs/swagger';
import { User } from '../../database/entities/user.entity';
import { UserDTO } from '../../users/models/user.dto';

export class InvestDebtRequestDTO {

    @ApiModelProperty()
    @Publish()
    id: string;

    @ApiModelProperty()
    @Publish()
    proposed_amount: number;

    @ApiModelProperty()
    @Publish()
    proposed_period_months: number;

    @ApiModelProperty()
    @Publish()
    proposed_interest: number;

    @ApiModelProperty()
    @Publish()
    penalty_rate: number;

    @ApiModelProperty()
    @Publish()
    proposal_status: InvesmentProposalStatus;

    @ApiModelProperty()
    @Publish(UserDTO)
    user: User;
}
