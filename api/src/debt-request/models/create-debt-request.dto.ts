import { IsNotEmpty, IsNumber, IsBoolean, IsString, Min } from 'class-validator';

export class CreateDebtRequestDTO {

    @IsNumber()
    @Min(1)
    @IsNotEmpty()
    target_amount: number;

    @IsNumber()
    @Min(1)
    @IsNotEmpty()
    target_period_months: number;

    @IsString()
    @IsNotEmpty()
    date: string;
}
