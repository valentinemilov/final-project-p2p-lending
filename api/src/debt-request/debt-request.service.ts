import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InvestmentProposal } from './../database/entities/investment-proposal.entity';
import { CreateDebtRequestDTO } from './models/create-debt-request.dto';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';
import { UserDebtRequestDTO } from './models/user-debt-request.dto';
import { DebtRequest } from '../database/entities/debt-request.entity';
import { User } from '../database/entities/user.entity';
import { UpdateDebtRequestDTO } from './models/update-debt-request.dto';
import { InvesmentProposalStatus } from '../common/enums/investment-proposal-status.enum';

@Injectable()
export class DebtRequestService {
    public constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(DebtRequest) private readonly debtRequestsRepository: Repository<DebtRequest>,
        @InjectRepository(InvestmentProposal) private readonly investmentProposalRepository: Repository<InvestmentProposal>,
    ) { }

    public async getAllDebtRequests(user: UserDebtRequestDTO): Promise<DebtRequest[]> {
        const foundUser: User = await this.findUserById(user.id);
        const allDebtRequestsData: DebtRequest[] = await this.debtRequestsRepository.find({
            where: {
                user: foundUser,
                isDeleted: false,
                isClosed: false,
            },
            relations: ['investment_proposals', 'investment_proposals.user'],
        });

        return allDebtRequestsData;
    }

    public async createDebtRequest(
        debtRequest: CreateDebtRequestDTO,
        user: UserDebtRequestDTO,
    ): Promise<DebtRequest> {
        const newDebtRequest: DebtRequest = this.debtRequestsRepository.create(debtRequest);
        const foundUser: User = await this.findUserById(user.id);

        newDebtRequest.user = Promise.resolve(foundUser);
        const savedDebtRequest: DebtRequest = await this.debtRequestsRepository.save(newDebtRequest);

        return savedDebtRequest;
    }

    public async updateDebtRequest(
        debtId: string,
        debtRequest: UpdateDebtRequestDTO,
        user: UserDebtRequestDTO,
    ): Promise<DebtRequest> {
        const foundUser: User = await this.findUserById(user.id);
        const debtRequestToUpdate: DebtRequest = await this.findDebtRequestById(debtId);
        const foundUserInDebtRequest: User = await debtRequestToUpdate.user;
        if (foundUser.id !== foundUserInDebtRequest.id) {
            throw new P2PLendingSystemError('The debt request does not belong to this user');
        }

        const investmentProposals: InvestmentProposal[] = await debtRequestToUpdate.investment_proposals;
        const filteredInvestmentProposals = investmentProposals
            .filter((curr: InvestmentProposal) => curr.proposed_amount > debtRequest.target_amount)
            .map(async (el: InvestmentProposal) => {
                el.proposal_status = InvesmentProposalStatus.Rejected;
                await this.investmentProposalRepository.save(el);
            });

        const updatedDebtRequest: DebtRequest = { ...debtRequestToUpdate, ...debtRequest };
        const savedDebtRequest: DebtRequest = await this.debtRequestsRepository.save(updatedDebtRequest);

        return savedDebtRequest;
    }

    public async deleteDebtRequest(
        debtId: string,
        user: UserDebtRequestDTO,
    ): Promise<DebtRequest> {
        const foundUser: User = await this.findUserById(user.id);
        const foundDebtRequest: DebtRequest = await this.findDebtRequestById(debtId);
        const foundUserInDebtRequest: User = await foundDebtRequest.user;
        if (foundUser.id !== foundUserInDebtRequest.id) {
            throw new P2PLendingSystemError('The debt request does not belong to this user');
        }

        const investmentProposals: InvestmentProposal[] = await foundDebtRequest.investment_proposals;
        const filteredInvestmentProposals = investmentProposals
            .map(async (el: InvestmentProposal) => {
                el.proposal_status = InvesmentProposalStatus.Rejected;
                await this.investmentProposalRepository.save(el);
            });

        const debtRequestToDelete = { ...foundDebtRequest, isDeleted: true };
        const deletedDebtRequest = await this.debtRequestsRepository.save(debtRequestToDelete);

        return deletedDebtRequest;
    }

    public async getOpenDebtRequests(userId: string): Promise<DebtRequest[]> {
        const allDebtRequestsData: DebtRequest[] = await this.debtRequestsRepository.find({
            where: {
                isClosed: false,
                isDeleted: false,
            },
            relations: ['user'],
        });

        return allDebtRequestsData.filter((curr) => {
            return (curr as any)['__user__'].id !== userId;
        });
    }

    private async findUserById(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('The user is not found', 400);
        }

        return foundUser;
    }

    private async findDebtRequestById(debtId: string): Promise<DebtRequest> {
        const foundDebtRequest: DebtRequest = await this.debtRequestsRepository.findOne(
            {
                where: {
                    id: debtId,
                },
                relations: ['user', 'investment_proposals'],
            },
        );
        if (foundDebtRequest === undefined || foundDebtRequest.isDeleted) {
            throw new P2PLendingSystemError('The debt request is not found', 400);
        }

        return foundDebtRequest;
    }
}
