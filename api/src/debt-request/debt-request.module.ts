import { Module } from '@nestjs/common';
import { DebtRequestController } from './debt-request.controller';
import { DebtRequestService } from './debt-request.service';
import { InvestmentProposal } from '../database/entities/investment-proposal.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { DebtRequest } from '../database/entities/debt-request.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DebtRequest, User, InvestmentProposal])],
  controllers: [DebtRequestController],
  providers: [DebtRequestService],
})
export class DebtRequestModule { }
