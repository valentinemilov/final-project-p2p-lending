import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  Post,
  Body,
  UseGuards,
  ValidationPipe,
  Put,
  Param,
  Delete,
  UsePipes,
} from '@nestjs/common';

import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { DebtRequestDTO } from './models/debt-request.dto';
import { DebtRequestService } from './debt-request.service';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { UserDebtRequestDTO } from './models/user-debt-request.dto';
import { CreateDebtRequestDTO } from './models/create-debt-request.dto';
import { UserDecorator } from '../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { DebtRequest } from '../database/entities/debt-request.entity';
import { UpdateDebtRequestDTO } from './models/update-debt-request.dto';
import { OpenDebtRequestDTO } from './models/open-debt-requests.dto';

@ApiUseTags('Debt-request Controller')
@ApiBearerAuth()
@UseGuards(AuthGuardWithBlacklisting)
@Controller('debtrequest')
export class DebtRequestController {
  public constructor(private readonly debtRequestService: DebtRequestService) { }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(DebtRequestDTO))
  public async getAllDebtRequests(@UserDecorator() user: UserDebtRequestDTO): Promise<DebtRequest[]> {
    const debtRequests: DebtRequest[] = await this.debtRequestService.getAllDebtRequests(user);

    return debtRequests;
  }

  @Post()
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(new TransformInterceptor(OpenDebtRequestDTO))
  public async addNewDebtRequest(
    @Body() debtRequest: CreateDebtRequestDTO,
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<DebtRequest> {

    return await this.debtRequestService.createDebtRequest(debtRequest, user);
  }

  @Put('/:debtId')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(DebtRequestDTO))
  public async updateDebtRequest(
    @Param('debtId') debtRequestId: string,
    @Body() debtRequest: UpdateDebtRequestDTO,
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<DebtRequest> {

    return await this.debtRequestService.updateDebtRequest(debtRequestId, debtRequest, user);
  }

  @Delete('/:debtId')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(DebtRequestDTO))
  public async deleteDebtRequest(
    @Param('debtId') debtRequestId: string,
    @UserDecorator() user: UserDebtRequestDTO,
  ): Promise<DebtRequest> {

    return await this.debtRequestService.deleteDebtRequest(debtRequestId, user);
  }

  @Get('/investor')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(OpenDebtRequestDTO))
  public async getOpenDebtRequests(@UserDecorator() user: UserDebtRequestDTO): Promise<DebtRequest[]> {
    const debtRequests: DebtRequest[] = await this.debtRequestService.getOpenDebtRequests(user.id);

    return debtRequests;
  }
}
