import { Length, IsEmail, IsNotEmpty, Matches } from 'class-validator';

export class CreateUserDTO {

    @IsNotEmpty()
    @Length(4, 20)
    username: string;

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
        message:
            'The password must be minimum five characters, at least one letter and one number',
    })
    password: string;
}
