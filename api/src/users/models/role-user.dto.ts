import { Publish } from '../../transformer/decorators/publish';

export class RoleDTO {

    @Publish()
    name: string;
}
