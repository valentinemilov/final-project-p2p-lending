import { Publish } from '../../transformer/decorators/publish';
import { Role } from '../../database/entities/role.entity';
import { RoleDTO } from './role-user.dto';

export class ReturnUserDTO {

    @Publish()
    id: string;

    @Publish()
    username: string;

    @Publish()
    email: string;

    @Publish()
    balance: number;

    @Publish(RoleDTO)
    role: Role;
}
