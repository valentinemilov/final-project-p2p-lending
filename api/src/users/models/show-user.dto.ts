export class ShowUserDTO {
    id: string;

    password: string;

    username: string;

    email: string;

    balance: number;
}
