import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class WithdrawDTO {

    @IsNotEmpty()
    @IsNumber()
    withdraw: number;
}
