import { IsNotEmpty } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class LoginUserDTO {

    @ApiModelProperty({example : 'Batman'})
    @IsNotEmpty()
    username: string;

    @ApiModelProperty({example : 'a1234'})
    @IsNotEmpty()
    password: string;
}
