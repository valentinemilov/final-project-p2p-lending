import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class DepositDTO {

    @IsNotEmpty()
    @IsNumber()
    deposit: number;
}
