import {
    Controller,
    Post,
    HttpCode,
    HttpStatus,
    Body,
    UseInterceptors,
    ValidationPipe,
    Put,
    UseGuards,
    Get,
    UsePipes,
} from '@nestjs/common';

import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { UsersService } from './users.service';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { ReturnUserDTO } from './models/return-user.dto';
import { ApiUseTags } from '@nestjs/swagger';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { DepositDTO } from './models/deposit.dto';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { WithdrawDTO } from './models/withdraw.dto';
import { UserDTO } from './models/user.dto';
import { User } from '../database/entities/user.entity';

@ApiUseTags('Users')
@Controller('users')
export class UsersController {
    public constructor(private readonly usersService: UsersService) { }

    @Get('')
    @HttpCode(HttpStatus.OK)
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(UserDTO))
    async getUser(
        @UserDecorator() user: UserDebtRequestDTO,
    ): Promise<User> {

        const foundUser = await this.usersService.getUser(user);

        return foundUser;
    }

    @Post()
    @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
    @HttpCode(HttpStatus.CREATED)
    @UseInterceptors(new TransformInterceptor(ReturnUserDTO))
    public async addNewUser(@Body() user: CreateUserDTO): Promise<ShowUserDTO> {

        return await this.usersService.createUser(user);
    }

    @Put('deposit')
    @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
    @HttpCode(HttpStatus.CREATED)
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ReturnUserDTO))
    public async deposit(
        @Body() deposit: DepositDTO,
        @UserDecorator() user: UserDebtRequestDTO,
    ): Promise<ShowUserDTO> {

        return await this.usersService.deposit(deposit, user);
    }

    @Put('withdraw')
    @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
    @HttpCode(HttpStatus.CREATED)
    @UseGuards(AuthGuardWithBlacklisting)
    @UseInterceptors(new TransformInterceptor(ReturnUserDTO))
    public async withdraw(
        @Body() withdraw: WithdrawDTO,
        @UserDecorator() user: UserDebtRequestDTO,
    ): Promise<ShowUserDTO> {

        return await this.usersService.withdraw(withdraw, user);
    }
}
