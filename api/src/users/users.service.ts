import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { LoginUserDTO } from './models/login-user.dto';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';
import { Role } from '../database/entities/role.entity';
import { UserRole } from '../common/enums/user-role.enum';
import { DepositDTO } from './models/deposit.dto';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { WithdrawDTO } from './models/withdraw.dto';
import { UserDTO } from './models/user.dto';

@Injectable()
export class UsersService {
    public constructor(
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
        @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    ) { }

    async getUser(user: UserDTO): Promise<User> {
        const foundUser = await this.findUserById(user.id);

        return foundUser;
    }

    public async createUser(user: CreateUserDTO): Promise<ShowUserDTO> {
        const roleEntity: Role = await this.rolesRepository.findOne({ where: { name: UserRole.Base } });
        const savedRole: Role = await this.rolesRepository.save(roleEntity);

        const searchedUser: User = await this.usersRepository.findOne({
            username: user.username,
            email: user.email,
        });

        if (searchedUser) {
            throw new P2PLendingSystemError('The username or email already exists', 400);
        }

        const userEntity: User = this.usersRepository.create(user);
        userEntity.password = await bcrypt.hash(user.password, 10);
        userEntity.role = Promise.resolve(savedRole);
        const savedUser: User = await this.usersRepository.save(userEntity);

        return savedUser;
    }

    async deposit(deposit: DepositDTO, user: UserDebtRequestDTO): Promise<User> {

        const foundUser: User = await this.findUserById(user.id);
        foundUser.balance += deposit.deposit;
        const savedInvestmentProposal = await this.usersRepository.save(foundUser);

        return savedInvestmentProposal;
    }

    async withdraw(withdraw: WithdrawDTO, user: UserDebtRequestDTO): Promise<User> {

        const foundUser: User = await this.findUserById(user.id);
        foundUser.balance -= withdraw.withdraw;
        const savedInvestmentProposal = await this.usersRepository.save(foundUser);

        return savedInvestmentProposal;
    }

    public async findUserByUsername(username: string): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                username,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('User with such username does not exist', 400);
        }

        return foundUser;
    }

    public async validateUserPassword(user: LoginUserDTO): Promise<boolean> {
        const userEntity: User = await this.usersRepository.findOne({
            username: user.username,
        });

        return await bcrypt.compare(user.password, userEntity.password);
    }

    private async findUserById(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('The user is not found', 400);
        }

        return foundUser;
    }
}
