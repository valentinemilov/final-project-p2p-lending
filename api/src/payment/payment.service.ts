import { Injectable } from '@nestjs/common';
import { LoanPayments } from '../database/entities/loan-payments.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePaymentsDTO } from './models/create-payment.dto';
import { InvestmentProposal } from '../database/entities/investment-proposal.entity';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { Loan } from '../database/entities/loan.entity';
import { SavePaymentsDTO } from './models/save-payment.dto';
import { User } from '../database/entities/user.entity';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';

@Injectable()
export class PaymentService {

    constructor(
        @InjectRepository(LoanPayments) private readonly paymentsRepository: Repository<LoanPayments>,
        @InjectRepository(Loan) private readonly loansRepository: Repository<Loan>,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    public async create(newPayment: CreatePaymentsDTO, user: UserDebtRequestDTO): Promise<LoanPayments> {

        const foundLoan: Loan = await this.findLoanByID(newPayment.loanId);
        const foundUser: User = await this.findUserById(user.id);
        const paymentToBeCreated: SavePaymentsDTO = {

            amount_paid: newPayment.amount_paid,

            interest_paid: newPayment.interest_paid,

            overdue_paid: newPayment.overdue_paid,

            payment_date: newPayment.payment_date,

        };

        const createdPayment: LoanPayments = this.paymentsRepository.create(paymentToBeCreated);
        createdPayment.loan = Promise.resolve(foundLoan);

        const savedInvestmentProposal = await this.paymentsRepository.save(createdPayment);

        const foundInvestor = await foundLoan.investor;
        foundInvestor.balance += paymentToBeCreated.amount_paid;
        await this.usersRepository.save(foundInvestor);

        foundUser.balance -= paymentToBeCreated.amount_paid;
        await this.usersRepository.save(foundUser);

        const foundLoanAfterUpdate: Loan = await this.findLoanByID(newPayment.loanId);
        const payments = await foundLoanAfterUpdate.payments;
        const amountPaid = await payments.reduce((accumulator, currentValue) => {
            return (accumulator + currentValue.amount_paid);
        }, 0);
        if (amountPaid === foundLoan.amount || amountPaid > foundLoan.amount) {
            foundLoanAfterUpdate.isClosed = true;
            this.loansRepository.save(foundLoanAfterUpdate);
        }

        return savedInvestmentProposal;
    }

    private async findLoanByID(loanId: string): Promise<Loan> {
        const allLoans = await this.loansRepository.findOne({
            where: {
                id: loanId,
                isDeleted: false,
            },
            relations: ['payments'],
        });

        return allLoans;
    }

    private async findUserById(userId: string) {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id: userId,
                isDeleted: false,
            },
        });

        if (!foundUser) {
            throw new P2PLendingSystemError('The user is not found', 400);
        }

        return foundUser;
    }
}
