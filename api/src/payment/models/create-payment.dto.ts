import { IsString, IsNotEmpty, IsNumber, Min } from 'class-validator';

export class CreatePaymentsDTO {

    @IsString()
    @IsNotEmpty()
    loanId: string;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    amount_paid: number;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    interest_paid: number;

    @IsNumber()
    @IsNotEmpty()
    @Min(0)
    overdue_paid: number;

    @IsString()
    @IsNotEmpty()
    payment_date: string;
}
