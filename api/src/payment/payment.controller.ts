import {
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  Body,
  Param,
  ValidationPipe,
  UsePipes,
  UseGuards,
} from '@nestjs/common';

import { PaymentService } from './payment.service';
import { TransformInterceptor } from '../transformer/interceptors/transform.interceptor';
import { LoanPaymentsDTO } from '../loans/models/loan-payments.dto';
import { CreatePaymentsDTO } from './models/create-payment.dto';
import { UserDecorator } from '../common/decorators/user.decorator';
import { UserDebtRequestDTO } from '../debt-request/models/user-debt-request.dto';
import { LoanPayments } from '../database/entities/loan-payments.entity';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';

@ApiUseTags('Payments Controller')
@ApiBearerAuth()
@UseGuards(AuthGuardWithBlacklisting)
@Controller('')
export class PaymentController {

  constructor(
    private readonly paymentService: PaymentService,
  ) { }

  @Post('loans/:loanId/payments')
  @UsePipes(new ValidationPipe({ whitelist: true, transform: true }))
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(new TransformInterceptor(LoanPaymentsDTO))
  async create(
    @Body() newPayment: CreatePaymentsDTO,
    @UserDecorator() user: UserDebtRequestDTO,
    @Param('loanId') loanId: string,
  ): Promise<LoanPayments> {

    return await this.paymentService.create(newPayment, user);
  }
}
