import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { PaymentController } from './payment.controller';
import { Loan } from '../database/entities/loan.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LoanPayments } from '../database/entities/loan-payments.entity';
import { User } from '../database/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([LoanPayments, User, Loan])],
  providers: [PaymentService],
  controllers: [PaymentController],
})
export class PaymentModule {}
