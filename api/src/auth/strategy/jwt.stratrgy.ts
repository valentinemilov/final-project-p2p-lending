import { Injectable } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from '../../config/config.service';
import { ShowUserDTO } from '../../users/models/show-user.dto';
import { UsersService } from '../../users/users.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly userDataService: UsersService,
        configService: ConfigService,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: configService.jwtSecret,
            ignoreExpiration: false,
        });
    }

    public async validate(payload: ShowUserDTO): Promise<ShowUserDTO> {
        const user = await this.userDataService.findUserByUsername(
            payload.username,
        );

        if (!user) {
            throw new Error(`Not authorized!`);
        }

        return user;
    }
}
