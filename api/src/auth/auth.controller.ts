import {
    Controller,
    Post,
    Body,
    Delete,
    UseGuards,
    ValidationPipe,
} from '@nestjs/common';

import { Token } from '../common/decorators/token.decorator';
import { AuthService } from './auth.service';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@ApiUseTags('Auth Controller')
@ApiBearerAuth()
@Controller('session')
export class AuthController {
    public constructor(private readonly authService: AuthService) { }

    @Post()
    public async loginUser(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: LoginUserDTO) {

        return await this.authService.login(user);
    }

    @Delete()
    @UseGuards(AuthGuardWithBlacklisting)
    public async logoutUser(@Token() token: string) {
        this.authService.blacklistToken(token);

        return {
            msg: 'Successful logout!',
        };
    }
}
