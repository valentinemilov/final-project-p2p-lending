import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDTO } from './../users/models/login-user.dto';
import { UsersService } from '../users/users.service';
import { P2PLendingSystemError } from '../common/exceptions/p2p-lending-system.error';
import { JwtPayload } from '../common/types/jwt-payload';
import { User } from '../database/entities/user.entity';

@Injectable()
export class AuthService {
    private readonly blacklist: string[] = [];

    public constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) { }
    public async login(user: LoginUserDTO): Promise<any> {
        const foundUser: User = await this.usersService.findUserByUsername(
            user.username,
        );

        if (!foundUser) {
            throw new P2PLendingSystemError(
                'User with such username does not exist!',
                400,
            );
        }

        if (!(await this.usersService.validateUserPassword(user))) {
            throw new P2PLendingSystemError('Invalid password!', 400);
        }

        const payload = ({
            id: foundUser.id,
            username: foundUser.username,
            role: (await foundUser.role).name,
        } as JwtPayload);

        return {
            token: await this.jwtService.signAsync(payload),
        };
    }

    public blacklistToken(token: string): void {
        this.blacklist.push(token);
    }

    public isTokenBlacklisted(token: string): boolean {

        return this.blacklist.includes(token);
    }
}
