import { Module, ValidationPipe } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { CoreModule } from './common/core.module';
import { DatabaseModule } from './database/database.module';
import { DebtRequestModule } from './debt-request/debt-request.module';
import { InvestmentProposalModule } from './investment-proposal/investment-proposal.module';
import { LoansModule } from './loans/loans.module';
import { PaymentModule } from './payment/payment.module';
import { APP_PIPE } from '@nestjs/core';

@Module({
  imports: [UsersModule,
    DatabaseModule,
    CoreModule,
    DebtRequestModule,
    InvestmentProposalModule,
    LoansModule,
    PaymentModule],
  controllers: [AppController],
  providers: [AppService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule { }
