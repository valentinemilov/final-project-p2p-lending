import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
} from 'typeorm';
import { User } from './user.entity';
import { InvesmentProposalStatus } from '../../common/enums/investment-proposal-status.enum';
import { DebtRequest } from './debt-request.entity';
import { Loan } from './loan.entity';

@Entity('investment_proposals')
export class InvestmentProposal {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'float', nullable: false})
    proposed_amount: number;

    @Column({ type: 'integer', nullable: false})
    proposed_period_months: number;

    @Column({ type: 'float', nullable: false })
    proposed_interest: number;

    @Column({ type: 'float', nullable: false })
    penalty_rate: number;

    @Column({ type: 'boolean', default: false, nullable: false })
    isDeleted: boolean;

    @Column({ type: 'enum', enum: InvesmentProposalStatus, default: InvesmentProposalStatus.Pending })
    proposal_status: InvesmentProposalStatus;

    @Column({ type: 'date', nullable: false })
    creation_date: string;

    @ManyToOne(type => User, user => user.investments)
    user: Promise<User>;

    @ManyToOne(type => DebtRequest, debt => debt.investment_proposals)
    debt: Promise<DebtRequest>;

    @ManyToOne(type => Loan, loan => loan.investment_proposals)
    loan: Promise<Loan>;
}
