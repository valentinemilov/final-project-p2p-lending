import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
} from 'typeorm';
import { Loan } from './loan.entity';

@Entity('loan_payments')
export class LoanPayments {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'float', nullable: false})
    amount_paid: number;

    @Column({ type: 'float', nullable: false})
    interest_paid: number;

    @Column({ type: 'float', nullable: false, default: 0})
    overdue_paid: number;

    @Column({ type: 'date', nullable: false })
    payment_date: string;

    @Column({ type: 'boolean', default: false, nullable: false })
    isDeleted: boolean;

    @ManyToOne(type => Loan, loan => loan.payments)
    loan: Promise<Loan>;

}
