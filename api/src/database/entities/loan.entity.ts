import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToOne,
    JoinColumn,
    OneToMany,
    ManyToOne,
} from 'typeorm';
import { DebtRequest } from './debt-request.entity';
import { InvestmentProposal } from './investment-proposal.entity';
import { LoanPayments } from './loan-payments.entity';
import { User } from './user.entity';

@Entity('loans')
export class Loan {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'integer', nullable: false})
    amount: number;

    @Column({ type: 'integer', nullable: false})
    period_months: number;

    @Column({ type: 'float', nullable: false })
    annual_interest: number;

    @Column({ type: 'date', nullable: false })
    creation_date: string;

    @Column({ type: 'float', nullable: false })
    penalty_rate: number;

    @Column({ type: 'boolean', default: false, nullable: false })
    isClosed: boolean;

    @Column({ type: 'boolean', default: false, nullable: false })
    isDeleted: boolean;

    @OneToOne(type => DebtRequest)
    @JoinColumn()
    debt_request: Promise<DebtRequest>;

    @OneToMany(type => InvestmentProposal, investment => investment.loan)
    investment_proposals: Promise<InvestmentProposal[]>;

    @OneToMany(type => LoanPayments, payment => payment.loan)
    payments: Promise<LoanPayments[]>;

    @ManyToOne(type => User, user => user.investments)
    investor: Promise<User>;

    @ManyToOne(type => User, user => user.debts)
    borrower: Promise<User>;
}
