import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    ManyToOne,
} from 'typeorm';
import { DebtRequest } from './debt-request.entity';
import { InvestmentProposal } from './investment-proposal.entity';
import { Role } from './role.entity';
import { Loan } from './loan.entity';

@Entity('users')
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'nvarchar', unique: true, nullable: false, length: 20 })
    username: string;

    @Column({ type: 'nvarchar', unique: true, nullable: false, length: 40 })
    email: string;

    @Column({ type: 'nvarchar', nullable: false, length: 80 })
    password: string;

    @Column({ type: 'float', default: 0, nullable: false })
    balance: number;

    @Column({ type: 'boolean', default: false, nullable: false })
    isDeleted: boolean;

    @ManyToOne(type => Role, role => role.users)
    role: Promise<Role>;

    @OneToMany(type => DebtRequest, debt => debt.user)
    debt_requests: Promise<DebtRequest[]>;

    @OneToMany(type => InvestmentProposal, investment => investment.user)
    investment_proposals: Promise<InvestmentProposal[]>;

    @OneToMany(type => Loan, loan => loan.borrower)
    debts: Promise<Loan[]>;

    @OneToMany(type => Loan, loan => loan.investor)
    investments: Promise<Loan[]>;
}
