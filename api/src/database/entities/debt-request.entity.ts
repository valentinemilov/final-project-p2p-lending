import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    OneToMany,
    ManyToOne,
    OneToOne,
} from 'typeorm';
import { User } from './user.entity';
import { InvestmentProposal } from './investment-proposal.entity';
import { Loan } from './loan.entity';

@Entity('debt_requests')
export class DebtRequest {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'float', nullable: false })
    target_amount: number;

    @Column({ type: 'integer', nullable: false })
    target_period_months: number;

    @Column({ type: 'date', nullable: false })
    date: string;

    @Column({ type: 'boolean', default: false, nullable: false })
    partial: boolean;

    @Column({ type: 'boolean', default: false, nullable: false })
    isClosed: boolean;

    @Column({ type: 'boolean', default: false, nullable: false })
    isDeleted: boolean;

    @ManyToOne(type => User, user => user.debts)
    user: Promise<User>;

    @OneToMany(type => InvestmentProposal, investment_proposals => investment_proposals.debt)
    investment_proposals: Promise<InvestmentProposal[]>;

    @OneToOne(type => Loan)
    loan: Promise<Loan>;
}
