import { createConnection, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { User } from '../entities/user.entity';
import { DebtRequest } from '../entities/debt-request.entity';
import { InvestmentProposal } from '../entities/investment-proposal.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { Role } from '../entities/role.entity';
import { Loan } from '../entities/loan.entity';
import { LoanPayments } from '../entities/loan-payments.entity';

const seed = async () => {
    const connection = await createConnection();
    const usersRepository: Repository<User> = connection.getRepository(User);
    const rolesRepository: Repository<Role> = connection.getRepository(Role);
    const debtsRepository: Repository<DebtRequest> = connection.getRepository(DebtRequest);
    const investmentsRepository: Repository<InvestmentProposal> = connection.getRepository(InvestmentProposal);
    const loansRepository: Repository<Loan> = connection.getRepository(Loan);
    const paymentsRepository: Repository<LoanPayments> = connection.getRepository(LoanPayments);

    // Creating roles

    const adminRole = await rolesRepository.save({
        name: UserRole.Admin,
    });

    const basicRole = await rolesRepository.save({
        name: UserRole.Base,
    });

    // Creating the first admin

    const howard = usersRepository.create();

    howard.username = 'Howard';
    howard.email = 'howard@abv.bg';
    howard.password = await bcrypt.hash('a1234', 10);
    howard.role = Promise.resolve(adminRole);

    await usersRepository.save(howard);

    // Creating users

    const batman = usersRepository.create();

    batman.username = 'Batman';
    batman.email = 'batman@abv.bg';
    batman.password = await bcrypt.hash('a1234', 10);
    batman.balance = 9200;
    batman.role = Promise.resolve(basicRole);

    await usersRepository.save(batman);

    const joker = usersRepository.create();

    joker.username = 'Joker';
    joker.email = 'joker@abv.bg';
    joker.password = await bcrypt.hash('a1234', 10);
    joker.balance = 15060;
    joker.role = Promise.resolve(basicRole);

    await usersRepository.save(joker);

    const robin = usersRepository.create();

    robin.username = 'Robin';
    robin.email = 'robin@abv.bg';
    robin.password = await bcrypt.hash('a1234', 10);
    robin.balance = 10000;
    robin.role = Promise.resolve(basicRole);

    await usersRepository.save(robin);

    // Creating debt

    const debtJoker10k = debtsRepository.create();

    debtJoker10k.target_amount = 10000;
    debtJoker10k.target_period_months = 12;
    debtJoker10k.date = '2019-11-20';
    debtJoker10k.user = Promise.resolve(joker);

    await debtsRepository.save(debtJoker10k);

    const debtJoker20k = debtsRepository.create();

    debtJoker20k.target_amount = 20000;
    debtJoker20k.target_period_months = 24;
    debtJoker20k.date = '2019-11-20';
    debtJoker20k.user = Promise.resolve(joker);

    await debtsRepository.save(debtJoker20k);

    const debtJoker30k = debtsRepository.create();

    debtJoker30k.target_amount = 30000;
    debtJoker30k.target_period_months = 24;
    debtJoker30k.date = '2019-11-20';
    debtJoker30k.user = Promise.resolve(joker);

    await debtsRepository.save(debtJoker30k);

    const debtJoker12k = debtsRepository.create();

    debtJoker12k.target_amount = 12000;
    debtJoker12k.target_period_months = 24;
    debtJoker12k.date = '2019-11-20';
    debtJoker12k.user = Promise.resolve(joker);

    await debtsRepository.save(debtJoker12k);

    const debtBatman10k = debtsRepository.create();

    debtBatman10k.target_amount = 10000;
    debtBatman10k.target_period_months = 12;
    debtBatman10k.date = '2019-11-20';
    debtBatman10k.user = Promise.resolve(batman);

    await debtsRepository.save(debtBatman10k);

    const debtBatman12k = debtsRepository.create();

    debtBatman12k.target_amount = 12000;
    debtBatman12k.target_period_months = 12;
    debtBatman12k.date = '2019-11-20';
    debtBatman12k.user = Promise.resolve(batman);

    await debtsRepository.save(debtBatman12k);

    const debtBatman30k = debtsRepository.create();

    debtBatman30k.target_amount = 30000;
    debtBatman30k.target_period_months = 12;
    debtBatman30k.date = '2019-10-17';
    debtBatman30k.user = Promise.resolve(batman);

    await debtsRepository.save(debtBatman30k);

    const debtBatman120k = debtsRepository.create();

    debtBatman120k.target_amount = 120000;
    debtBatman120k.target_period_months = 48;
    debtBatman120k.date = '2019-07-17';
    debtBatman120k.user = Promise.resolve(batman);

    await debtsRepository.save(debtBatman120k);

    const debtRobin20k = debtsRepository.create();

    debtRobin20k.target_amount = 20000;
    debtRobin20k.target_period_months = 24;
    debtRobin20k.date = '2019-08-17';
    debtRobin20k.user = Promise.resolve(robin);

    await debtsRepository.save(debtRobin20k);

    const debtRobin36k = debtsRepository.create();

    debtRobin36k.target_amount = 36000;
    debtRobin36k.target_period_months = 36;
    debtRobin36k.date = '2019-08-17';
    debtRobin36k.user = Promise.resolve(robin);

    await debtsRepository.save(debtRobin36k);

    const debtRobin5k = debtsRepository.create();

    debtRobin5k.target_amount = 5000;
    debtRobin5k.target_period_months = 12;
    debtRobin5k.date = '2019-08-17';
    debtRobin5k.user = Promise.resolve(robin);

    await debtsRepository.save(debtRobin5k);

    // Creating investment proposals

    const investJoker10k = investmentsRepository.create();

    investJoker10k.proposed_amount = 10000;
    investJoker10k.proposed_period_months = 12;
    investJoker10k.proposed_interest = 0.032;
    investJoker10k.penalty_rate = 0.01;
    investJoker10k.creation_date = '2019-07-19';
    investJoker10k.user = Promise.resolve(joker);
    investJoker10k.debt = Promise.resolve(debtBatman10k);

    await investmentsRepository.save(investJoker10k);

    const investBatman10k = investmentsRepository.create();

    investBatman10k.proposed_amount = 10000;
    investBatman10k.proposed_period_months = 12;
    investBatman10k.proposed_interest = 0.032;
    investBatman10k.penalty_rate = 0.01;
    investBatman10k.creation_date = '2019-07-19';
    investBatman10k.user = Promise.resolve(batman);
    investBatman10k.debt = Promise.resolve(debtJoker10k);

    await investmentsRepository.save(investBatman10k);

    const investRobin30k = investmentsRepository.create();

    investRobin30k.proposed_amount = 30000;
    investRobin30k.proposed_period_months = 12;
    investRobin30k.proposed_interest = 0.032;
    investRobin30k.penalty_rate = 0.009;
    investRobin30k.creation_date = '2019-08-19';
    investRobin30k.user = Promise.resolve(robin);
    investRobin30k.debt = Promise.resolve(debtBatman30k);

    await investmentsRepository.save(investRobin30k);

    const investJoker20k = investmentsRepository.create();

    investJoker20k.proposed_amount = 20000;
    investJoker20k.proposed_period_months = 24;
    investJoker20k.proposed_interest = 0.032;
    investJoker20k.penalty_rate = 0.011;
    investJoker20k.creation_date = '2019-06-17';
    investJoker20k.user = Promise.resolve(joker);
    investJoker20k.debt = Promise.resolve(debtRobin20k);

    await investmentsRepository.save(investJoker20k);

    const investJoker120k = investmentsRepository.create();

    investJoker120k.proposed_amount = 12000;
    investJoker120k.proposed_period_months = 24;
    investJoker120k.proposed_interest = 0.032;
    investJoker120k.penalty_rate = 0.012;
    investJoker120k.creation_date = '2019-06-17';
    investJoker120k.user = Promise.resolve(joker);
    investJoker120k.debt = Promise.resolve(debtBatman10k);

    await investmentsRepository.save(investJoker120k);

    // Creating loans

    const loanBatmanRobin = loansRepository.create();

    loanBatmanRobin.amount = 30000;
    loanBatmanRobin.period_months = 12;
    loanBatmanRobin.annual_interest = 0.032;
    loanBatmanRobin.creation_date = '2019-08-14';
    loanBatmanRobin.penalty_rate = investRobin30k.penalty_rate;
    loanBatmanRobin.investor = Promise.resolve(robin);
    loanBatmanRobin.borrower = Promise.resolve(batman);
    loanBatmanRobin.debt_request = Promise.resolve(debtBatman30k);
    loanBatmanRobin.investment_proposals = Promise.resolve([investRobin30k]);
    debtBatman30k.isClosed = true;
    investRobin30k.proposal_status = 2;
    const totalInterestloanBatmanRobin = Number(((((loanBatmanRobin.amount *
        (loanBatmanRobin.annual_interest / 12) *
        loanBatmanRobin.period_months) /
        (1 - Math.pow((1 + (loanBatmanRobin.annual_interest / 12)), -loanBatmanRobin.period_months)))) - loanBatmanRobin.amount)
        .toFixed(2));

    await loansRepository.save(loanBatmanRobin);
    await debtsRepository.save(debtBatman30k);
    await investmentsRepository.save(investRobin30k);

    const loanRobinJokerOverdue = loansRepository.create();

    loanRobinJokerOverdue.amount = 20000;
    loanRobinJokerOverdue.period_months = 24;
    loanRobinJokerOverdue.annual_interest = 0.08;
    loanRobinJokerOverdue.creation_date = '2019-06-03';
    loanRobinJokerOverdue.penalty_rate = investJoker20k.penalty_rate;
    loanRobinJokerOverdue.investor = Promise.resolve(joker);
    loanRobinJokerOverdue.borrower = Promise.resolve(batman);
    loanRobinJokerOverdue.debt_request = Promise.resolve(debtRobin20k);
    loanRobinJokerOverdue.investment_proposals = Promise.resolve([investJoker20k]);
    debtRobin20k.isClosed = true;
    investJoker20k.proposal_status = 2;
    const totalInterestloanRobinJokerOverdue = Number(((((loanRobinJokerOverdue.amount *
        (loanRobinJokerOverdue.annual_interest / 12) *
        loanRobinJokerOverdue.period_months) /
        (1 - Math.pow((1 + (loanRobinJokerOverdue.annual_interest / 12)), -loanRobinJokerOverdue.period_months)))) - loanRobinJokerOverdue.amount)
        .toFixed(2));

    await loansRepository.save(loanRobinJokerOverdue);
    await debtsRepository.save(debtRobin20k);
    await investmentsRepository.save(investJoker20k);

    const loanBatmanJokerOverdue = loansRepository.create();

    loanBatmanJokerOverdue.amount = 12000;
    loanBatmanJokerOverdue.period_months = 24;
    loanBatmanJokerOverdue.annual_interest = 0.08;
    loanBatmanJokerOverdue.creation_date = '2019-06-07';
    loanBatmanJokerOverdue.penalty_rate = investJoker120k.penalty_rate;
    loanBatmanJokerOverdue.investor = Promise.resolve(joker);
    loanBatmanJokerOverdue.borrower = Promise.resolve(batman);
    loanBatmanJokerOverdue.debt_request = Promise.resolve(debtBatman120k);
    loanBatmanJokerOverdue.investment_proposals = Promise.resolve([investJoker120k, investRobin30k]);
    debtBatman120k.isClosed = true;
    investJoker120k.proposal_status = 2;
    const totalInterestloanBatmanJokerOverdue = Number(((((loanBatmanJokerOverdue.amount *
        (loanBatmanJokerOverdue.annual_interest / 12) *
        loanBatmanJokerOverdue.period_months) /
        (1 - Math.pow((1 + (loanBatmanJokerOverdue.annual_interest / 12)), -loanBatmanJokerOverdue.period_months)))) -
        loanBatmanJokerOverdue.amount)
        .toFixed(2));

    await loansRepository.save(loanBatmanJokerOverdue);
    await debtsRepository.save(debtBatman120k);
    await investmentsRepository.save(investJoker120k);

    const loanJokerBatmanOverdue = loansRepository.create();

    loanJokerBatmanOverdue.amount = 10000;
    loanJokerBatmanOverdue.period_months = 24;
    loanJokerBatmanOverdue.annual_interest = 0.08;
    loanJokerBatmanOverdue.creation_date = '2019-09-10';
    loanJokerBatmanOverdue.penalty_rate = investJoker10k.penalty_rate;
    loanJokerBatmanOverdue.investor = Promise.resolve(batman);
    loanJokerBatmanOverdue.borrower = Promise.resolve(joker);
    loanJokerBatmanOverdue.debt_request = Promise.resolve(debtJoker10k);
    loanJokerBatmanOverdue.investment_proposals = Promise.resolve([investBatman10k]);
    debtBatman10k.isClosed = true;
    investBatman10k.proposal_status = 2;
    const totalInterestloanJokerBatmanOverdue = Number(((((loanJokerBatmanOverdue.amount *
        (loanJokerBatmanOverdue.annual_interest / 12) *
        loanJokerBatmanOverdue.period_months) /
        (1 - Math.pow((1 + (loanJokerBatmanOverdue.annual_interest / 12)), -loanJokerBatmanOverdue.period_months)))) -
        loanJokerBatmanOverdue.amount)
        .toFixed(2));

    await loansRepository.save(loanJokerBatmanOverdue);
    await debtsRepository.save(debtJoker10k);
    await investmentsRepository.save(investBatman10k);

    // Creating payments

    const payment1loanBatmanRobin = paymentsRepository.create();

    payment1loanBatmanRobin.amount_paid = Number((loanBatmanRobin.amount / loanBatmanRobin.period_months).toFixed(2));
    payment1loanBatmanRobin.interest_paid = Number((totalInterestloanBatmanRobin / loanBatmanRobin.period_months).toFixed(2));
    payment1loanBatmanRobin.payment_date = '2019-09-10';
    payment1loanBatmanRobin.overdue_paid = 0;
    payment1loanBatmanRobin.loan = Promise.resolve(loanBatmanRobin);

    await paymentsRepository.save(payment1loanBatmanRobin);

    const payment2loanBatmanRobin = paymentsRepository.create();

    payment2loanBatmanRobin.amount_paid = Number((loanBatmanRobin.amount / loanBatmanRobin.period_months).toFixed(2));
    payment2loanBatmanRobin.interest_paid = Number((totalInterestloanBatmanRobin / loanBatmanRobin.period_months).toFixed(2));
    payment2loanBatmanRobin.payment_date = '2019-10-10';
    payment2loanBatmanRobin.overdue_paid = 0;
    payment2loanBatmanRobin.loan = Promise.resolve(loanBatmanRobin);

    await paymentsRepository.save(payment2loanBatmanRobin);

    const payment3loanBatmanRobin = paymentsRepository.create();

    payment3loanBatmanRobin.amount_paid = Number((loanBatmanRobin.amount / loanBatmanRobin.period_months).toFixed(2));
    payment3loanBatmanRobin.interest_paid = Number((totalInterestloanBatmanRobin / loanBatmanRobin.period_months).toFixed(2));
    payment3loanBatmanRobin.payment_date = '2019-11-10';
    payment3loanBatmanRobin.overdue_paid = 0;
    payment3loanBatmanRobin.loan = Promise.resolve(loanBatmanRobin);

    await paymentsRepository.save(payment3loanBatmanRobin);

    const payment1loanBatmanJokerOverdue = paymentsRepository.create();

    payment1loanBatmanJokerOverdue.amount_paid = Number((loanBatmanJokerOverdue.amount / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment1loanBatmanJokerOverdue.interest_paid = Number((totalInterestloanBatmanJokerOverdue / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment1loanBatmanJokerOverdue.payment_date = '2019-07-14';
    payment1loanBatmanJokerOverdue.overdue_paid = 0;
    payment1loanBatmanJokerOverdue.loan = Promise.resolve(loanBatmanJokerOverdue);

    await paymentsRepository.save(payment1loanBatmanJokerOverdue);

    const payment2loanBatmanJokerOverdue = paymentsRepository.create();

    payment2loanBatmanJokerOverdue.amount_paid = Number((loanBatmanJokerOverdue.amount / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment2loanBatmanJokerOverdue.interest_paid = Number((totalInterestloanBatmanJokerOverdue / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment2loanBatmanJokerOverdue.payment_date = '2019-08-14';
    payment2loanBatmanJokerOverdue.overdue_paid = 0;
    payment2loanBatmanJokerOverdue.loan = Promise.resolve(loanBatmanJokerOverdue);

    await paymentsRepository.save(payment2loanBatmanJokerOverdue);

    const payment3loanBatmanJokerOverdue = paymentsRepository.create();

    payment3loanBatmanJokerOverdue.amount_paid = Number((loanBatmanJokerOverdue.amount / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment3loanBatmanJokerOverdue.interest_paid = Number((totalInterestloanBatmanJokerOverdue / loanBatmanJokerOverdue.period_months).toFixed(2));
    payment3loanBatmanJokerOverdue.payment_date = '2019-09-14';
    payment3loanBatmanJokerOverdue.overdue_paid = 0;
    payment3loanBatmanJokerOverdue.loan = Promise.resolve(loanBatmanJokerOverdue);

    await paymentsRepository.save(payment3loanBatmanJokerOverdue);

    const payment1loanRobinJokerOverdue = paymentsRepository.create();

    payment1loanRobinJokerOverdue.amount_paid = Number((loanRobinJokerOverdue.amount / loanRobinJokerOverdue.period_months).toFixed(2));
    payment1loanRobinJokerOverdue.interest_paid = Number((totalInterestloanRobinJokerOverdue / loanRobinJokerOverdue.period_months).toFixed(2));
    payment1loanRobinJokerOverdue.payment_date = '2019-09-14';
    payment1loanRobinJokerOverdue.overdue_paid = 0;
    payment1loanRobinJokerOverdue.loan = Promise.resolve(loanRobinJokerOverdue);

    await paymentsRepository.save(payment1loanRobinJokerOverdue);

    const payment2loanRobinJokerOverdue = paymentsRepository.create();

    payment2loanRobinJokerOverdue.amount_paid = Number((loanRobinJokerOverdue.amount / loanRobinJokerOverdue.period_months).toFixed(2));
    payment2loanRobinJokerOverdue.interest_paid = Number((totalInterestloanRobinJokerOverdue / loanRobinJokerOverdue.period_months).toFixed(2));
    payment2loanRobinJokerOverdue.payment_date = '2019-10-14';
    payment2loanRobinJokerOverdue.overdue_paid = 0;
    payment2loanRobinJokerOverdue.loan = Promise.resolve(loanRobinJokerOverdue);

    await paymentsRepository.save(payment2loanRobinJokerOverdue);

    const paymentloanJokerBatmanOverdue = paymentsRepository.create();

    paymentloanJokerBatmanOverdue.amount_paid = Number((loanJokerBatmanOverdue.amount / loanJokerBatmanOverdue.period_months).toFixed(2));
    paymentloanJokerBatmanOverdue.interest_paid = Number((totalInterestloanJokerBatmanOverdue / loanJokerBatmanOverdue.period_months).toFixed(2));
    paymentloanJokerBatmanOverdue.payment_date = '2019-10-14';
    paymentloanJokerBatmanOverdue.overdue_paid = 0;
    paymentloanJokerBatmanOverdue.loan = Promise.resolve(loanJokerBatmanOverdue);

    await paymentsRepository.save(paymentloanJokerBatmanOverdue);

    const payment2loanJokerBatmanOverdue = paymentsRepository.create();

    payment2loanJokerBatmanOverdue.amount_paid = Number((loanJokerBatmanOverdue.amount / loanJokerBatmanOverdue.period_months).toFixed(2));
    payment2loanJokerBatmanOverdue.interest_paid = Number((totalInterestloanJokerBatmanOverdue / loanJokerBatmanOverdue.period_months).toFixed(2));
    payment2loanJokerBatmanOverdue.payment_date = '2019-11-14';
    payment2loanJokerBatmanOverdue.overdue_paid = 0;
    payment2loanJokerBatmanOverdue.loan = Promise.resolve(loanJokerBatmanOverdue);

    await paymentsRepository.save(payment2loanJokerBatmanOverdue);

    // tslint:disable-next-line: no-console
    console.log(`Data seeded successfully`);
};

// tslint:disable-next-line: no-console
seed().catch(console.error);
