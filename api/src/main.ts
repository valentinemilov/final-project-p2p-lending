import { P2PLendingSystemErrorFilter } from './common/filter/p2p-lending-error.filter';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import helmet from 'helmet';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.use(helmet());

  const options = new DocumentBuilder()
    .setTitle('P2P-Lending')
    .setDescription('The P2P-Lending API description')
    .setVersion('1.0')
    .addTag('P2P-Lending')
    .addBearerAuth()
    .build();

  app.useGlobalFilters(new P2PLendingSystemErrorFilter());

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}

bootstrap();
