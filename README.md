# Peer-to-Peer Lending Platform

### Project Description

Peer-to-Peer Lending Platform is a single-page application, which connects people who need money with people willing to lend money. The system allows each registered user to behave as both investor and borrower. They can get information about their current loans, investments and available funds any time they want.

---

### Getting Started

These instructions will get you a copy of the project and allow you to run it on your local machine for development and testing purposes.

#### Server

After you clone successfully this repository:

- navigate to the `api` folder

- create a **database** in MySQL Workbench. The default name is _p2p-lending_ and if you would like to rename it it, you must change it the .env config file

- create `.env` file at root level- it contains sensitive data about your server. DB_USERNAME and DB_PASSWORD are the ones set in your MySQL Workbench

```sh
PORT=3000
DB_TYPE=mysql
DB_HOST=localhost
DB_PORT=3306
DB_USERNAME=root
DB_PASSWORD=root
DB_DATABASE_NAME=p2p-lending
JWT_SECRET=randomkey
JWT_EXPIRE_TIME=86400
```

- create ormconfig.json file at root level

```sh
{
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "username": "root",
    "password": "root",
    "database": "p2p-lending",
    "synchronize": "true",
    "entities": [
        "src/database/entities/**/*.ts"
    ],
    "migrations": [
        "src/database/migration/**/*.ts"
    ],
    "cli": {
        "entitiesDir": "src/database/entities",
        "migrationsDir": "src/database/migration"
    }
}
```

- open the terminal or bash at root level and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  - to populate the database:
    $ npm run seed
  ```

  ```sh
  $ npm run start
  ```

#### Swagger

After runnig your server successfully, you can use Swagger on http://localhost:3000/api/ to check the endpoints.

#### Client

##### Having successfully run the server, you can run the application

- navigate to the `client` folder

- open the terminal and run the following commands:

  ```sh
  $ npm install
  ```

  ```sh
  $ ng serve --o
  ```

  ```sh
  You can either create your own users, or just login with the existing ones:
  - username: Batman, password: a1234
  - username: Joker, password: a1234
  ```

---

#### Known Issues

- Edit-in-place component is used to update debt requests and investment proposals. It has no front-end validations. It would have been a better idea to use modals and reactive forms
- When making a payment on loan, reloading the page is required

---

#### Testing



```sh
$ ng test
```

---

### Technologies

- Angular 8
- NestJS
- Angular Material
- TypeORM

---

### Authors and Contributors

- [Stanislav Trifonov](https://gitlab.com/stanislav_trifonov)
- [Valentin Ivanov](https://gitlab.com/valentinemilov)

---

### License

This project is licensed under the **[MIT](https://choosealicense.com/licenses/mit/)** License
