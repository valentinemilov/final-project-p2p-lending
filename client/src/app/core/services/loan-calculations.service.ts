import { Injectable } from '@angular/core';
import { LoanDTO } from '../../loans/models/loans.dto';
import * as moment from 'moment';
import { LoanStatus } from '../../shared/enums/loan-status.enum';

@Injectable({
  providedIn: 'root'
})
export class LoanCalculationsService {

  constructor() { }

  public totalLoanInterestAmount(loan: LoanDTO): number {
    const totalInterest = ((((loan.amount * (loan.annual_interest / 12) * loan.period_months)
      / (1 - Math.pow((1 + (loan.annual_interest / 12)), -loan.period_months)))) - loan.amount);

    return totalInterest;
  }

  public monthlyLoanInstallment(loan: LoanDTO): number {

    return loan.amount / loan.period_months;
  }

  public monthlyInterestAmount(loan: LoanDTO, totalLoanInterestAmount: number): number {

    return totalLoanInterestAmount / loan.period_months;
  }

  public installmentsPaid(loan: LoanDTO): number {

    return loan.payments.length;
  }

  public installmentsLeft(loan: LoanDTO, installmentsPaid: number): number {

    return loan.period_months - installmentsPaid;
  }

  public loanAmountPaid(loan: LoanDTO): number {
    const loanAmountPaid = loan.payments.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.amount_paid;
    }, 0);

    return loanAmountPaid;
  }

  public loanAmountLeft(loan: LoanDTO, loanAmountPaid: number): number {

    return loan.amount - loanAmountPaid;
  }

  public interestAmountPaid(monthlyInterestAmount: number, installmentsPaid: number): number {

    return monthlyInterestAmount * installmentsPaid;
  }

  public interestAmountLeft(totalLoanInterestAmount: number, interestAmountPaid: number): number {

    return totalLoanInterestAmount - interestAmountPaid;
  }

  public totalAmountPaid(loanAmountPaid: number, interestAmountPaid: number): number {

    return loanAmountPaid + interestAmountPaid;
  }

  public totalAmountLeft(loanAmountLeft: number, interestAmountLeft: number): number {

    return loanAmountLeft + interestAmountLeft;
  }

  public dateToday(): string {

    return moment(new Date()).format('YYYY-MM-DD');
  }

  public currentInstallmentDueDate(loan: LoanDTO): string {

    return moment(loan.creation_date, 'YYYY-MM-DD').add(loan.payments.length + 1, 'M').format('YYYY-MM-DD');
  }

  public overdueDays(dateToday: string, currentInstallmentDueDate: string): number {

    const a = moment(dateToday, 'YYYY-MM-DD');
    const b = moment(currentInstallmentDueDate, 'YYYY-MM-DD');
    const diffDays = a.diff(b, 'days');
    let days: number;
    if (diffDays >= 0) {
      days = diffDays;
    } else {
      days = 0;
    }

    return days;
  }

  public amountOverdue(loan: LoanDTO, overdueDays: number, monthlyLoanInstallment: number) {

    return loan.penalty_rate * overdueDays * monthlyLoanInstallment;
  }

  public installmentsOverdue(dateToday: string, currentInstallmentDueDate: string): number {
    const today = moment(dateToday, 'YYYY-MM-DD');
    const diffMonths = today.diff(currentInstallmentDueDate, 'months');
    const currentMonthInstallmentCheck = today.diff(currentInstallmentDueDate, 'days');

    let installments: number;
    if (diffMonths >= 0) {
      installments = diffMonths;
    } else {
      installments = 0;
    }

    if (currentMonthInstallmentCheck > 0) {
      installments += 1;
    }

    return installments;
  }

  public loanStatusCheck(daysOverdue: number): LoanStatus {
    if (daysOverdue > 0) {

      return LoanStatus.Overdue;
    } else {

      return LoanStatus.Regular;
    }
  }

  public monthlyTotalAmount(monthlyLoanInstallment: number, monthlyInterestAmount: number, amountOverdue: number): number {

    return monthlyLoanInstallment + monthlyInterestAmount + amountOverdue;
  }
}
