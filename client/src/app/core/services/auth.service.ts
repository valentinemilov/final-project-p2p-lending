
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { NotificationService } from './notification.service';
import { UserDTO } from '../../users/models/user.dto';
import { UserRegisterDTO } from '../../users/models/user-register.dto';
import { CONFIG } from '../../config/config';
import { UserCredentialsDTO } from '../../users/models/user-credentials.dto';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService,
    private readonly notificator: NotificationService
  ) { }

  public get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: UserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): UserDTO {
    const token: string = this.storage.read('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.delete('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  // include the return type
  public register(user: UserRegisterDTO) {
    return this.http.post(
      `${CONFIG.BASE_URL}/users`,
      user
    );
  }

  public login(user: UserCredentialsDTO): Observable<any> {
    return this.http
      .post<{ token: string }>(`${CONFIG.BASE_URL}/session`, user)
      .pipe(
        tap(({ token }) => {

          this.storage.save('token', token);
          const userData: UserDTO = this.jwtService.decodeToken(token);

          this.emitUserData(userData);
        })
      );
  }

  public logout(): void {
    this.storage.delete('token');
    this.emitUserData(null);
    this.notificator.success(`Logged out successfully!`);
  }
}
