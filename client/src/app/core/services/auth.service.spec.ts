import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UserDTO } from '../../users/models/user.dto';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { NotificationService } from './notification.service';
import { CONFIG } from '../../config/config';
import { UserCredentialsDTO } from '../../users/models/user-credentials.dto';
import { UserRegisterDTO } from '../../users/models/user-register.dto';

describe('AuthService', () => {
    let httpClient;
    let storageService;
    let jwtService;
    let notificator;

    let service: AuthService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            post() { },
            delete() { }
        };

        storageService = {
            read() { },
            save() { },
            delete() { },
            clear() { }
        };

        jwtService = {
            isTokenExpired() { },
            decodeToken() { }
        };

        notificator = {
            success() { },
            error() { }
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule, JwtModule.forRoot({ config: {} })],
            providers: [AuthService, StorageService, NotificationService]
        })
            .overrideProvider(HttpClient, { useValue: httpClient })
            .overrideProvider(StorageService, { useValue: storageService })
            .overrideProvider(JwtHelperService, { useValue: jwtService })
            .overrideProvider(NotificationService, { useValue: notificator });

        service = TestBed.get(AuthService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getUserDataIfAuthenticated()', () => {
        it('should call the storage.read() method once with correct parameter', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const readSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'clear');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(false);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            service.getUserDataIfAuthenticated();

            // Assert
            expect(readSpy).toBeCalledTimes(1);
            expect(readSpy).toBeCalledWith('token');
        });

        it('should call the jwtService.isTokenExpired() method once with correct token if the token exists', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(false);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            service.getUserDataIfAuthenticated();

            // Assert
            expect(isTokenExpiredSpy).toBeCalledTimes(1);
            expect(isTokenExpiredSpy).toBeCalledWith(fakeToken);
        });

        it('should call the storage.removeItem() method once with correct parameter if the token exists as is expired', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(true);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            service.getUserDataIfAuthenticated();

            // Assert
            expect(removeItemSpy).toBeCalledTimes(1);
            expect(removeItemSpy).toBeCalledWith('token');
        });

        it('should call the jwtService.decodeToken() method once with correct parameter if the token exists as is not expired', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(false);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            service.getUserDataIfAuthenticated();

            // Assert
            expect(decodeTokenSpy).toBeCalledTimes(1);
            expect(decodeTokenSpy).toBeCalledWith(fakeToken);
        });

        it('should return the value of jwtService.decodeToken() if the token exists as is not expired', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(false);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            const returnValue = service.getUserDataIfAuthenticated();

            // Assert
            expect(returnValue).toEqual(mockedUserData);
        });

        it('should null if the token does not exist', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(null);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(false);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            const returnValue = service.getUserDataIfAuthenticated();

            // Assert
            expect(returnValue).toEqual(null);
        });

        it('should null if the token exists but is expired', () => {
            // Arrange
            const mockedUserData: UserDTO = {
                id: '1',
                username: 'Batman',
                email: 'batmant@g.com',
                balance: 1000,
            };

            const fakeToken = 'fake token';

            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(fakeToken);

            const removeItemSpy = jest.spyOn(storageService, 'delete');

            const isTokenExpiredSpy = jest
                .spyOn(jwtService, 'isTokenExpired')
                .mockReturnValue(true);

            const decodeTokenSpy = jest
                .spyOn(jwtService, 'decodeToken')
                .mockReturnValue(mockedUserData);

            // Act
            const returnValue = service.getUserDataIfAuthenticated();

            // Assert
            expect(returnValue).toEqual(null);
        });
    });

    describe('register()', () => {
        it('should call the http.post() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/users`;

            const mockedUserCredentials: UserRegisterDTO = {
                username: 'Batman',
                email: 'batmant@g.com',
                password: 'password'
            };

            const mockedUserData = 'mocked user data';

            // Mock this in order to initialize the loggedUserDataSubject$
            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(undefined);

            const spy = jest
                .spyOn(httpClient, 'post')
                .mockReturnValue(of(mockedUserData));

            // Act & Assert
            service.register(mockedUserCredentials).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, mockedUserCredentials);

                done();
            });
        });

        it('should return the http.post() method return value', done => {
            // Arrange
            const mockedUserCredentials: UserRegisterDTO = {
                username: 'Batman',
                email: 'batmant@g.com',
                password: 'password'
            };

            const mockedUserData = 'mocked user data';

            // Mock this in order to initialize the loggedUserDataSubject$
            const getItemSpy = jest
                .spyOn(storageService, 'read')
                .mockReturnValue(undefined);

            const spy = jest
                .spyOn(httpClient, 'post')
                .mockReturnValue(of(mockedUserData));

            // Act & Assert
            service.register(mockedUserCredentials).subscribe((returnValue: any) => {
                expect(returnValue).toEqual(mockedUserData);

                done();
            });
        });

        describe('login()', () => {
            it('should call the http.post() method once with correct parameters', done => {
                // Arrange
                const url = `${CONFIG.BASE_URL}/session`;

                const mockedUserCredentials: UserCredentialsDTO = {
                    username: 'pesho',
                    password: 'password'
                };

                const fakeToken = { token: 'fake token' };
                const mockedUserData = 'mocked user data';

                // Mock this in order to initialize the loggedUserDataSubject$
                const getItemSpy = jest
                    .spyOn(storageService, 'read')
                    .mockReturnValue(undefined);

                const postSpy = jest
                    .spyOn(httpClient, 'post')
                    .mockReturnValue(of(fakeToken));

                const setItemSpy = jest.spyOn(storageService, 'save');

                const decodeTokenSpy = jest
                    .spyOn(jwtService, 'decodeToken')
                    .mockReturnValue(mockedUserData);

                // Act & Assert
                service.login(mockedUserCredentials).subscribe(() => {
                    expect(postSpy).toBeCalledTimes(1);
                    expect(postSpy).toBeCalledWith(url, mockedUserCredentials);

                    done();
                });
            });

            it('should call the storage.setItem() method once with correct token', done => {
                // Arrange
                const url = `${CONFIG.BASE_URL}/session`;

                const mockedUserCredentials: UserCredentialsDTO = {
                    username: 'pesho',
                    password: 'password'
                };

                const fakeToken = { token: 'fake token' };
                const mockedUserData = 'mocked user data';

                // Mock this in order to initialize the loggedUserDataSubject$
                const getItemSpy = jest
                    .spyOn(storageService, 'read')
                    .mockReturnValue(undefined);

                const postSpy = jest
                    .spyOn(httpClient, 'post')
                    .mockReturnValue(of(fakeToken));

                const setItemSpy = jest.spyOn(storageService, 'save');

                const decodeTokenSpy = jest
                    .spyOn(jwtService, 'decodeToken')
                    .mockReturnValue(mockedUserData);

                // Act & Assert
                service.login(mockedUserCredentials).subscribe(() => {
                    expect(setItemSpy).toBeCalledTimes(1);
                    expect(setItemSpy).toBeCalledWith('token', fakeToken.token);

                    done();
                });
            });

            it('should call the jwtService.decodeToken() method once with correct token', done => {
                // Arrange
                const url = `${CONFIG.BASE_URL}/session`;

                const mockedUserCredentials: UserCredentialsDTO = {
                    username: 'pesho',
                    password: 'password'
                };

                const fakeToken = { token: 'fake token' };
                const mockedUserData = 'mocked user data';

                // Mock this in order to initialize the loggedUserDataSubject$
                const getItemSpy = jest
                    .spyOn(storageService, 'read')
                    .mockReturnValue(undefined);

                const postSpy = jest
                    .spyOn(httpClient, 'post')
                    .mockReturnValue(of(fakeToken));

                const setItemSpy = jest.spyOn(storageService, 'save');

                const decodeTokenSpy = jest
                    .spyOn(jwtService, 'decodeToken')
                    .mockReturnValue(mockedUserData);

                // Act & Assert
                service.login(mockedUserCredentials).subscribe(() => {
                    expect(decodeTokenSpy).toBeCalledTimes(1);
                    expect(decodeTokenSpy).toBeCalledWith(fakeToken.token);

                    done();
                });
            });

            it('should emit the decoded user data from the internal subject', done => {
                // Arrange
                const url = `${CONFIG.BASE_URL}/session`;

                const mockedUserCredentials: UserCredentialsDTO = {
                    username: 'pesho',
                    password: 'password'
                };

                const fakeToken = { token: 'fake token' };
                const mockedUserData = 'mocked user data';

                // Mock this in order to initialize the loggedUserDataSubject$
                const getItemSpy = jest
                    .spyOn(storageService, 'read')
                    .mockReturnValue(undefined);

                const postSpy = jest
                    .spyOn(httpClient, 'post')
                    .mockReturnValue(of(fakeToken));

                const setItemSpy = jest.spyOn(storageService, 'save');

                const decodeTokenSpy = jest
                    .spyOn(jwtService, 'decodeToken')
                    .mockReturnValue(mockedUserData);

                // Act
                service.login(mockedUserCredentials).subscribe();

                // Assert
                service.loggedUserData$.subscribe(data => {
                    expect(data).toEqual(mockedUserData);

                    done();
                });
            });

            it('should return the http.post() method return value', done => {
                // Arrange
                const url = `${CONFIG.BASE_URL}/session`;

                const mockedUserCredentials: UserCredentialsDTO = {
                    username: 'pesho',
                    password: 'password'
                };

                const fakeToken = { token: 'fake token' };
                const mockedUserData = 'mocked user data';

                // Mock this in order to initialize the loggedUserDataSubject$
                const getItemSpy = jest
                    .spyOn(storageService, 'read')
                    .mockReturnValue(undefined);

                const postSpy = jest
                    .spyOn(httpClient, 'post')
                    .mockReturnValue(of(fakeToken));

                const setItemSpy = jest.spyOn(storageService, 'save');

                const decodeTokenSpy = jest
                    .spyOn(jwtService, 'decodeToken')
                    .mockReturnValue(mockedUserData);

                // Act & Assert
                service.login(mockedUserCredentials).subscribe((returnValue: any) => {
                    expect(returnValue).toEqual(fakeToken);

                    done();
                });
            });

            describe('logout()', () => {
                it('logout() should call storageService.delete', () => {

                    const token = '1';

                    const spy = jest.spyOn(storageService, 'delete').mockImplementation(() => { });

                    // const service = getService();

                    service.logout();

                    expect(storageService.delete).toHaveBeenCalledWith('token');

                });
            });
        });
    });
});
