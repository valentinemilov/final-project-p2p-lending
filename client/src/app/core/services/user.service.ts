import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DepositDTO } from '../../dashboard/models/deposit.dto';
import { Observable } from 'rxjs';
import { UserDTO } from '../../users/models/user.dto';
import { CONFIG } from '../../config/config';
import { WithdrawDTO } from '../../dashboard/models/withdraw.dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public constructor(private readonly http: HttpClient) { }

  public getUser(): Observable<UserDTO> {

    return this.http.get<UserDTO>(`${CONFIG.BASE_URL}/users`);
  }

  public deposit(deposit: DepositDTO): Observable<UserDTO> {

    return this.http.put<UserDTO>(`${CONFIG.BASE_URL}/users/deposit`, deposit);
  }

  public withdraw(withdraw: WithdrawDTO): Observable<UserDTO> {

    return this.http.put<UserDTO>(`${CONFIG.BASE_URL}/users/withdraw`, withdraw);
  }
}
