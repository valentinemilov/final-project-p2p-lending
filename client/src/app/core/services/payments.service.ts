import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { CreatePaymentsDTO } from '../../loans/models/create-payment.dto';
import { LoanPaymentsDTO } from '../../loans/models/loan-payments.dto';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private readonly http: HttpClient) { }

  public create(newPayment: CreatePaymentsDTO): Observable<LoanPaymentsDTO> {

    return this.http.post<LoanPaymentsDTO>(`${CONFIG.BASE_URL}/loans/${newPayment.loanId}/payments`, newPayment);
  }
}
