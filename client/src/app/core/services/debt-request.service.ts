import { CreateDebtRequestDTO } from './../../debt-request/models/create-debt-request.dto';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';
import { DebtRequestDTO } from '../../debt-request/models/debt-request-dto';
import { OpenDebtRequestsDTO } from '../../investor/investment-proposal/models/open-debt-requests.dto';
import { UpdateDebtRequestDTO } from '../../debt-request/models/update-debt-request.dto';

@Injectable({
    providedIn: 'root'
})

export class DebtRequestService {
    public constructor(private readonly http: HttpClient) { }

    public getAllDebtRequests(): Observable<DebtRequestDTO[]> {

        return this.http.get<DebtRequestDTO[]>(`${CONFIG.BASE_URL}/debtrequest`);
    }

    public createDebtRequest(debtRequest: CreateDebtRequestDTO): Observable<OpenDebtRequestsDTO> {

        return this.http.post<OpenDebtRequestsDTO>(`${CONFIG.BASE_URL}/debtrequest`, debtRequest);
    }

    public deleteDebtRequest(debtRequestId: string): Observable<DebtRequestDTO> {

        return this.http.delete<DebtRequestDTO>(`${CONFIG.BASE_URL}/debtrequest/${debtRequestId}`);
    }

    public updateDebtRequest(debtRequest: UpdateDebtRequestDTO): Observable<DebtRequestDTO> {

        return this.http.put<DebtRequestDTO>(`${CONFIG.BASE_URL}/debtrequest/${debtRequest.id}`, debtRequest);
    }

    public getAllOpenDebtRequests(): Observable<OpenDebtRequestsDTO[]> {

        return this.http.get<OpenDebtRequestsDTO[]>(`${CONFIG.BASE_URL}/debtrequest/investor`);
    }
}
