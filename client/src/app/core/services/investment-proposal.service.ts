import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InvestmentProposalDTO } from '../../investor/investment-proposal/models/investment-proposal.dto';
import { CONFIG } from '../../config/config';
import { CreateInvestmentProposalDTO } from '../../investor/investment-proposal/models/create-investment-proposal.dto';
import { RejectInvestmentProposalDTO } from '../../debt-request/models/reject-investment-proposal.dto';
import { UpdateInvestmentProposalDTO } from '../../investor/investment-proposal/models/update-investment-proposal.dto';


@Injectable({
  providedIn: 'root'
})
export class InvestmentProposalService {

  public constructor(private readonly http: HttpClient) { }

  public getAll(): Observable<InvestmentProposalDTO[]> {

    return this.http.get<InvestmentProposalDTO[]>(`${CONFIG.BASE_URL}/investor/investmentproposal`);
  }

  public create(investmentProposal: CreateInvestmentProposalDTO, debtId: string): Observable<InvestmentProposalDTO> {

    return this.http.post<InvestmentProposalDTO>(`${CONFIG.BASE_URL}/debtrequest/${debtId}/investmentproposal`, investmentProposal);
  }

  public update(investmentProposal: UpdateInvestmentProposalDTO, debtId: string): Observable<InvestmentProposalDTO> {

    return this.http.put<InvestmentProposalDTO>(`${CONFIG.BASE_URL}/debtrequest/${debtId}/investmentproposal`, investmentProposal);
  }

  public delete(investmentProposalId: string): Observable<InvestmentProposalDTO> {

    return this.http.delete<InvestmentProposalDTO>(`${CONFIG.BASE_URL}/user/investmentproposal/${investmentProposalId}`);
  }

  public rejectInvestmentProposal(debtId: string, investmentProposalId: string): Observable<RejectInvestmentProposalDTO> {

    return this.http.put<RejectInvestmentProposalDTO>
      (`${CONFIG.BASE_URL}/debtrequest/${debtId}/investmentproposal/${investmentProposalId}`,
        investmentProposalId);
  }
}
