import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { CONFIG } from '../../config/config';
import { UserService } from './user.service';
import { DepositDTO } from '../../dashboard/models/deposit.dto';
import { WithdrawDTO } from '../../dashboard/models/withdraw.dto';

describe('UserService', () => {
    let httpClient;

    let service: UserService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            get() { },
            put() { },
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [UserService]
        }).overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(UserService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getUser()', () => {
        it('should call the httpClient.get() method once with correct parameters', done => {
            // Arrange

            const url = `${CONFIG.BASE_URL}/users`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getUser().subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        describe('deposit()', () => {
            it('should call the httpClient.put() method once with correct parameters', done => {
                // Arrange

                const mockedDeposit: DepositDTO = {
                    deposit: 1000,
                };

                const url = `${CONFIG.BASE_URL}/users/deposit`;
                const returnValue = of('return value');

                const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);


                // Act & Assert
                service.deposit(mockedDeposit).subscribe(() => {
                    expect(spy).toBeCalledTimes(1);
                    expect(spy).toBeCalledWith(url, mockedDeposit);

                    done();
                });
            });

            it('should return the result from the httpClient.put() method', () => {
                // Arrange
                const mockedWithdraw: WithdrawDTO = {
                    withdraw: 1000,
                };
                const url = `${CONFIG.BASE_URL}/users/withdraw`;
                const returnValue = of('return value');

                const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

                // Act
                const result = service.withdraw(mockedWithdraw);

                // Assert
                expect(result).toEqual(returnValue);
            });
        });
    });
});
