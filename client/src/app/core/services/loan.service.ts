import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoanDTO } from '../../loans/models/loans.dto';
import { Observable } from 'rxjs';
import { CONFIG } from '../../config/config';

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  constructor(private readonly http: HttpClient) { }

  public getAllInvestorLoans(): Observable<LoanDTO[]> {

    return this.http.get<LoanDTO[]>(`${CONFIG.BASE_URL}/loans/investor`);
  }

  public getAllBorrowerLoans(): Observable<LoanDTO[]> {

    return this.http.get<LoanDTO[]>(`${CONFIG.BASE_URL}/loans/borrower`);
  }

  public createNewLoan(debtRequestId: string, proposalId: string): Observable<LoanDTO> {

    return this.http.post<LoanDTO>(`${CONFIG.BASE_URL}/debtrequest/${debtRequestId}/investmentproposal/${proposalId}/loans`,
      debtRequestId);
  }
}
