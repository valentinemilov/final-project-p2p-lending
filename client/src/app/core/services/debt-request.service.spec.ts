import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { DebtRequestService } from './debt-request.service';
import { CONFIG } from '../../config/config';
import { CreateDebtRequestDTO } from '../../debt-request/models/create-debt-request.dto';
import { UpdateDebtRequestDTO } from '../../debt-request/models/update-debt-request.dto';

describe('DebtRequestService', () => {
    let httpClient;
    let service: DebtRequestService;

    beforeEach(async () => {
        jest.clearAllMocks();

        httpClient = {
            get() { },
            post() { },
            put() { },
            delete() { }
        };

        await TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [DebtRequestService]
        }).overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(DebtRequestService);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('getAllDebtRequests()', () => {
        it('should call the httpClient.get() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getAllDebtRequests().subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.get() method', () => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllDebtRequests();

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('createDebtRequest()', () => {
        it('should call the httpClient.post() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const debtRequest: CreateDebtRequestDTO = {
                target_amount: 1000,
                target_period_months: 1,
                date: '2019-12-12',
            };

            // Act & Assert
            service.createDebtRequest(debtRequest).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, debtRequest);

                done();
            });
        });

        it('should return the result from the httpClient.post() method', () => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);
            const debtRequest: CreateDebtRequestDTO = {
                target_amount: 1000,
                target_period_months: 1,
                date: '2019-12-12',
            };

            // Act
            const result = service.createDebtRequest(debtRequest);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('deleteDebtRequest()', () => {
        it('should call the httpClient.delete() method once with correct parameters', done => {
            // Arrange
            const debtRequestId = '123';
            const url = `${CONFIG.BASE_URL}/debtrequest/${debtRequestId}`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act & Assert
            service.deleteDebtRequest(debtRequestId).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.get() method', () => {
            // Arrange
            const debtRequestId = '123';
            const url = `${CONFIG.BASE_URL}/debtrequest/${debtRequestId}`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

            // Act
            const result = service.deleteDebtRequest(debtRequestId);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('updateDebtRequest()', () => {
        it('should call the httpClient.put() method once with correct parameters', done => {
            // Arrange
            const debtRequest: UpdateDebtRequestDTO = {
                id: '123',
                target_amount: 1000,
                target_period_months: 12,
            };

            const url = `${CONFIG.BASE_URL}/debtrequest/${debtRequest.id}`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

            // Act & Assert
            service.updateDebtRequest(debtRequest).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, debtRequest);

                done();
            });
        });

        it('should return the result from the httpClient.put() method', () => {
            // Arrange
            const debtRequest: UpdateDebtRequestDTO = {
                id: '123',
                target_amount: 1000,
                target_period_months: 12,
            };

            const url = `${CONFIG.BASE_URL}/debtrequest/${debtRequest.id}`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

            // Act
            const result = service.updateDebtRequest(debtRequest);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });

    describe('getAllOpenDebtRequests()', () => {
        it('should call the httpClient.get() method once with correct parameters', done => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest/investor`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act & Assert
            service.getAllOpenDebtRequests().subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url);

                done();
            });
        });

        it('should return the result from the httpClient.get() method', () => {
            // Arrange
            const url = `${CONFIG.BASE_URL}/debtrequest/investor`;
            const returnValue = of('string');

            const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

            // Act
            const result = service.getAllOpenDebtRequests();

            // Assert
            expect(result).toEqual(returnValue);
        });
    });
});
