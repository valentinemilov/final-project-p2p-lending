import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { PaymentService } from './payments.service';
import { CONFIG } from '../../config/config';
import { CreatePaymentsDTO } from '../../loans/models/create-payment.dto';

describe('PaymentsService', () => {
    let httpClient;

    let service: PaymentService;

    beforeEach(async(() => {
        jest.clearAllMocks();

        httpClient = {
            post() { },
        };

        TestBed.configureTestingModule({
            imports: [HttpClientModule],
            providers: [PaymentService]
        }).overrideProvider(HttpClient, { useValue: httpClient });

        service = TestBed.get(PaymentService);
    }));

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(service).toBeDefined();
    });

    describe('create()', () => {
        it('should call the httpClient.post() method once with correct parameters', done => {
            // Arrange
            const mockedNewPayment: CreatePaymentsDTO = {
                loanId: '1',
                amount_paid: 100,
                interest_paid: 12,
                overdue_paid: 0,
                payment_date: '2019-09-09'
            };

            const url = `${CONFIG.BASE_URL}/loans/${mockedNewPayment.loanId}/payments`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act & Assert
            service.create(mockedNewPayment).subscribe(() => {
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(url, mockedNewPayment);

                done();
            });
        });

        it('should return the result from the httpClient.post() method', () => {
            // Arrange
            const mockedNewPayment: CreatePaymentsDTO = {
                loanId: '1',
                amount_paid: 100,
                interest_paid: 12,
                overdue_paid: 0,
                payment_date: '2019-09-09'
            };
            const url = `${CONFIG.BASE_URL}/loans/${mockedNewPayment.loanId}/payments`;
            const returnValue = of('return value');

            const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

            // Act
            const result = service.create(mockedNewPayment);

            // Assert
            expect(result).toEqual(returnValue);
        });
    });
});
