import { NgModule, Optional, SkipSelf } from '@angular/core';
import { NotificationService } from './services/notification.service';
import { StorageService } from './services/storage.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { ToastrModule } from 'ngx-toastr';
import { InvestmentProposalService } from './services/investment-proposal.service';
import { LoanService } from './services/loan.service';
import { LoanCalculationsService } from './services/loan-calculations.service';


@NgModule({
  providers: [
    NotificationService,
    StorageService,
    AuthService,
    AuthGuard,
    InvestmentProposalService,
    LoanService,
    LoanCalculationsService,
  ],
  declarations: [],
  imports: [
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    })
  ],
  exports: [

  ],
})
export class CoreModule {

  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
