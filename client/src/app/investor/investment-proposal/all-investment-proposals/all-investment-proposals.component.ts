import { UpdateInvestmentProposalDTO } from './../models/update-investment-proposal.dto';
import { Component, OnInit } from '@angular/core';
import { InvestmentProposalDTO } from '../models/investment-proposal.dto';
import { InvestmentProposalService } from '../../../core/services/investment-proposal.service';
import { NotificationService } from '../../../core/services/notification.service';
import { ActivatedRoute } from '@angular/router';
import { UserDTO } from '../../../users/models/user.dto';

@Component({
  selector: 'app-all-investment-proposals',
  templateUrl: './all-investment-proposals.component.html',
  styleUrls: ['./all-investment-proposals.component.css']
})
export class AllInvestmentProposalsComponent implements OnInit {
  loggedUser: UserDTO;

  public investmentProposals: InvestmentProposalDTO[] = [];

  constructor(
    private readonly investmentProposalService: InvestmentProposalService,

    private readonly notificator: NotificationService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.route.data.subscribe(
      ({ proposals }) => {
        this.investmentProposals = proposals;
      });
  }

  public deleteProposal(investmentProposalId: string): void {

    this.investmentProposalService
      .delete(investmentProposalId)
      .subscribe(
        () => {
          this.investmentProposals = this.investmentProposals.filter(
            (investmentProposal: InvestmentProposalDTO) => investmentProposal.id !== investmentProposalId
          );
          this.notificator.success('Investment proposal successfully deleted!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }

  public updateProposal(investmentProposal: InvestmentProposalDTO): void {
    const debtId: string = investmentProposal.debt.id;
    const proposalToUpdate: UpdateInvestmentProposalDTO = {
      id: investmentProposal.id,
      proposed_interest: investmentProposal.proposed_interest,
      penalty_rate: investmentProposal.penalty_rate,
    };

    this.investmentProposalService
      .update(proposalToUpdate, debtId)
      .subscribe(
        () => {
          this.notificator.success('Investment proposal successfully updated!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }
}
