import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Routes, ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { SharedModule } from '../../../shared/shared.module';
import { DebtRequestService } from '../../../core/services/debt-request.service';
import { InvestmentProposalService } from '../../../core/services/investment-proposal.service';
import { NotificationService } from '../../../core/services/notification.service';
import { AllInvestmentProposalsComponent } from '../../investment-proposal/all-investment-proposals/all-investment-proposals.component';
import {
    InvestmentProposalDetailsComponent
} from '../../investment-proposal/investment-proposal-details/investment-proposal-details.component';
import { AllOpenDebtRequestsComponent } from '../../open-debt-requests/all-open-debt-requests/all-open-debt-requests.component';
import {
    CreateInvestmentProposalComponent
} from '../../open-debt-requests/create-investment-proposal/create-investment-proposal.component';
import {
    IndividualOpenDebtRequestsComponent
} from '../../open-debt-requests/individual-open-debt-requests/individual-open-debt-requests.component';
import { InvestorRouterModule } from '../../investor-routing.module';
import { InvestmentProposalDTO } from '../models/investment-proposal.dto';
import { InvesmentProposalStatus } from '../../../shared/enums/invesment-proposal-status.enum';

describe('AllInvestmentProposalsComponent', () => {
    let investmentProposalService;
    let notificator;
    let activatedRoute;

    let component: AllInvestmentProposalsComponent;
    let fixture: ComponentFixture<AllInvestmentProposalsComponent>;

    const routes: Routes = [
        { path: '', redirectTo: 'investor', pathMatch: 'full' },
        { path: 'investor', component: AllInvestmentProposalsComponent },
    ];

    beforeEach(async () => {
        jest.clearAllMocks();

        investmentProposalService = {
            getAll() { },
            create() { },
            update() { },
            delete() { },
        };

        notificator = {
            success() { },
            error() { }
        };

        activatedRoute = {
            data: of({
                investmentProposals: [],
            })
        };

        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
                InvestorRouterModule
            ],
            declarations: [
                AllInvestmentProposalsComponent,
                InvestmentProposalDetailsComponent,
                CreateInvestmentProposalComponent,
                AllOpenDebtRequestsComponent,
                IndividualOpenDebtRequestsComponent,
            ],
            providers: [DebtRequestService, InvestmentProposalService, NotificationService]
        })

            .overrideProvider(InvestmentProposalService, { useValue: investmentProposalService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
            .compileComponents();
        fixture = TestBed.createComponent(AllInvestmentProposalsComponent);
        component = fixture.componentInstance;

    });

    it('should be defined', () => {

        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit()', () => {

        it('should initialize correctly with the data passed from the resolver', () => {

            // Arrange
            const proposals: InvestmentProposalDTO[] = [{

                id: '1',
                proposed_amount: 1000,
                proposed_period_months: 12,
                proposed_interest: 0.03,
                penalty_rate: 0.03,
                proposal_status: InvesmentProposalStatus.Pending,
                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },
                debt: {
                    id: '1',
                    target_amount: 1000,
                    target_period_months: 12,
                    date: '2019-11-19',
                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },
                }

            }];
            activatedRoute.data = of({ proposals });
            fixture = TestBed.createComponent(AllInvestmentProposalsComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            expect(component.investmentProposals).toBe(proposals);
        });
    });

    describe('investmentProposalService.delete()', () => {

        it('should call the investmentProposalService.create() once with correct parameters', () => {

            // Arrange
            const mockedProposalData: InvestmentProposalDTO = {

                id: '1',
                proposed_amount: 1000,
                proposed_period_months: 12,
                proposed_interest: 0.03,
                penalty_rate: 0.03,
                proposal_status: InvesmentProposalStatus.Pending,
                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },
                debt: {
                    id: '1',
                    target_amount: 1000,
                    target_period_months: 12,
                    date: '2019-11-19',
                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },
                }
            };

            component.investmentProposals = [];
            const mockedReturnedData = 'data';

            const successSpy = jest.spyOn(notificator, 'success');

            const spy = jest
                .spyOn(investmentProposalService, 'delete')
                .mockReturnValue(of(mockedReturnedData));

            // Act
            component.deleteProposal(mockedProposalData.id);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(mockedProposalData.id);
        });

        it(`should subscribe to the investmentProposalService.create() observable and call
        notificationService.success() once when the response is successful`, () => {

                // Arrange
                const mockedProposalData: InvestmentProposalDTO = {

                    id: '1',
                    proposed_amount: 1000,
                    proposed_period_months: 12,
                    proposed_interest: 0.03,
                    penalty_rate: 0.03,
                    proposal_status: InvesmentProposalStatus.Pending,
                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },
                    debt: {
                        id: '1',
                        target_amount: 1000,
                        target_period_months: 12,
                        date: '2019-11-19',
                        user: {
                            id: '1',
                            username: 'Batman',
                            email: 'batman@b.com',
                            balance: 1000,
                        },
                    }

                };

                component.investmentProposals = [];
                const mockedReturnedData = 'data';

                const spy = jest
                    .spyOn(investmentProposalService, 'delete')
                    .mockReturnValue(of(mockedReturnedData));

                const successSpy = jest.spyOn(notificator, 'success');

                // Act
                component.deleteProposal(mockedProposalData.id);

                // Assert
                expect(notificator.success).toBeCalledTimes(1);
                expect(notificator.success).toBeCalledWith('Investment proposal successfully deleted!');
            });

        it(`should subscribe to the investmentProposalService.create() observable
        and call notificationService.error() once when the response is unsuccessful`, () => {

                // Arrange
                const mockedProposalData: InvestmentProposalDTO = {

                    id: '1',
                    proposed_amount: 1000,
                    proposed_period_months: 12,
                    proposed_interest: 0.03,
                    penalty_rate: 0.03,
                    proposal_status: InvesmentProposalStatus.Pending,
                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },
                    debt: {
                        id: '1',
                        target_amount: 1000,
                        target_period_months: 12,
                        date: '2019-11-19',
                        user: {
                            id: '1',
                            username: 'Batman',
                            email: 'batman@b.com',
                            balance: 1000,
                        },
                    }
                };

                component.investmentProposals = [];

                const createProposalSpy = jest
                    .spyOn(investmentProposalService, 'delete')
                    .mockReturnValue(throwError('Error!'));

                const errorSpy = jest.spyOn(notificator, 'error');

                // Act
                component.deleteProposal(mockedProposalData.id);

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid request!');
            });

        describe('investmentProposalService.updateProposal()', () => {
            it('should call the investmentProposalService.update() once with correct parameters', () => {
                // Arrange

                const updateProposalData = {
                    id: '1',
                    proposed_interest: 0.03,
                    penalty_rate: 0.03,
                };

                const mockedProposalData: InvestmentProposalDTO = {

                    id: '1',
                    proposed_amount: 1000,
                    proposed_period_months: 12,
                    proposed_interest: 0.03,
                    penalty_rate: 0.03,
                    proposal_status: InvesmentProposalStatus.Pending,
                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },
                    debt: {
                        id: '1',
                        target_amount: 1000,
                        target_period_months: 12,
                        date: '2019-11-19',
                        user: {
                            id: '3',
                            username: 'Batman',
                            email: 'batman@b.com',
                            balance: 1000,
                        },
                    }

                };

                component.investmentProposals = [];
                const mockedReturnedData = 'data';

                const successSpy = jest.spyOn(notificator, 'success');

                const spy = jest
                    .spyOn(investmentProposalService, 'update')
                    .mockReturnValue(of(mockedReturnedData));

                // Act
                component.updateProposal(mockedProposalData);

                // Assert
                expect(spy).toBeCalledTimes(1);
                expect(spy).toBeCalledWith(updateProposalData, mockedProposalData.debt.id);
            });

            it(`should subscribe to the investmentProposalService.updateProposal()
            observable and call notificationService.success() once when the response is successful`, () => {

                    // Arrange
                    const mockedProposalData: InvestmentProposalDTO = {

                        id: '1',
                        proposed_amount: 1000,
                        proposed_period_months: 12,
                        proposed_interest: 0.03,
                        penalty_rate: 0.03,
                        proposal_status: InvesmentProposalStatus.Pending,
                        user: {
                            id: '1',
                            username: 'Batman',
                            email: 'batman@b.com',
                            balance: 1000,
                        },
                        debt: {
                            id: '1',
                            target_amount: 1000,
                            target_period_months: 12,
                            date: '2019-11-19',
                            user: {
                                id: '1',
                                username: 'Batman',
                                email: 'batman@b.com',
                                balance: 1000,
                            },
                        }
                    };

                    component.investmentProposals = [];
                    const mockedReturnedData = 'data';

                    const spy = jest
                        .spyOn(investmentProposalService, 'update')
                        .mockReturnValue(of(mockedReturnedData));

                    const successSpy = jest.spyOn(notificator, 'success');

                    // Act
                    component.updateProposal(mockedProposalData);

                    // Assert
                    expect(notificator.success).toBeCalledTimes(1);
                    expect(notificator.success).toBeCalledWith('Investment proposal successfully updated!');
                });

            it(`should subscribe to the investmentProposalService.updateProposal() observable
        and call notificationService.error() once when the response is unsuccessful`, () => {

                    // Arrange
                    const mockedProposalData: InvestmentProposalDTO = {

                        id: '1',
                        proposed_amount: 1000,
                        proposed_period_months: 12,
                        proposed_interest: 0.03,
                        penalty_rate: 0.03,
                        proposal_status: InvesmentProposalStatus.Pending,
                        user: {
                            id: '1',
                            username: 'Batman',
                            email: 'batman@b.com',
                            balance: 1000,
                        },
                        debt: {
                            id: '1',
                            target_amount: 1000,
                            target_period_months: 12,
                            date: '2019-11-19',
                            user: {
                                id: '1',
                                username: 'Batman',
                                email: 'batman@b.com',
                                balance: 1000,
                            },
                        }
                    };
                    component.investmentProposals = [];

                    const createProposalSpy = jest
                        .spyOn(investmentProposalService, 'update')
                        .mockReturnValue(throwError('Error!'));

                    const errorSpy = jest.spyOn(notificator, 'error');

                    // Act
                    component.updateProposal(mockedProposalData);

                    // Assert
                    expect(errorSpy).toBeCalledTimes(1);
                    expect(errorSpy).toBeCalledWith('Invalid request!');
                });
        });
    });
});
