import { UserDTO } from '../../../users/models/user.dto';
import { InvestProposalDebtRequestDTO } from './invest-proposal-debt.dto';
import { InvesmentProposalStatus } from '../../../shared/enums/invesment-proposal-status.enum';

export class InvestmentProposalDTO {

    id: string;

    proposed_amount: number;

    proposed_period_months: number;

    proposed_interest: number;

    proposal_status: InvesmentProposalStatus;

    penalty_rate: number;

    user: UserDTO;

    debt: InvestProposalDebtRequestDTO;
}
