import { UserDTO } from '../../../users/models/user.dto';

export class OpenDebtRequestsDTO {
    id: string;

    target_amount: number;

    target_period_months: number;

    date: string;

    user: UserDTO;
}
