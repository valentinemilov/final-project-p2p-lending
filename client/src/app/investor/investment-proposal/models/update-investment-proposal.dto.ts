export class UpdateInvestmentProposalDTO {

    id: string;

    proposed_interest: number;

    penalty_rate: number;
}
