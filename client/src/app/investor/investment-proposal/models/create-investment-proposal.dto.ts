export class CreateInvestmentProposalDTO {

    debtId: string;

    proposed_amount: number;

    proposed_period_months: number;

    proposed_interest: number;

    creation_date: string;

    penalty_rate: number;
}
