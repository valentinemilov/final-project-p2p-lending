import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { InvestmentProposalDTO } from '../models/investment-proposal.dto';

@Component({
  selector: 'app-investment-proposal-details',
  templateUrl: './investment-proposal-details.component.html',
  styleUrls: ['./investment-proposal-details.component.css']
})
export class InvestmentProposalDetailsComponent implements OnInit {

  @Input() public investmentProposal: InvestmentProposalDTO;
  @Output() public readonly deleteProposal: EventEmitter<
    string
  > = new EventEmitter();

  interestedFormated: string;
  penaltyFormated: string;

  @Output() public readonly updateProposal: EventEmitter<
    InvestmentProposalDTO
  > = new EventEmitter();

  public ngOnInit() {
    const interestedFormated = (this.investmentProposal.proposed_interest * 100).toFixed(2);
    this.interestedFormated = interestedFormated;
    const penaltyFormated = (this.investmentProposal.penalty_rate * 100).toFixed(2);
    this.penaltyFormated = penaltyFormated;
  }

  public deleteInvestmentProposal(): void {
    const investmentProposalId = this.investmentProposal.id;
    this.deleteProposal.emit(investmentProposalId);
  }

  public updateProposalInterest(interest: number) {
    this.investmentProposal.proposed_interest = +interest / 100;
  }

  public updatePenaltyRate(rate: number) {
    this.investmentProposal.penalty_rate = +rate / 100;
  }

  public updateInvestmentProposal(): void {
    const investmentProposal = this.investmentProposal;
    this.updateProposal.emit(investmentProposal);
  }
}
