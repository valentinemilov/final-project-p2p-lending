import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { NotificationService } from '../core/services/notification.service';
import { map } from 'rxjs/operators';
import { InvestmentProposalService } from '../core/services/investment-proposal.service';
import { InvestmentProposalDTO } from './investment-proposal/models/investment-proposal.dto';

@Injectable()
export class InvestmentProposalResolverService implements Resolve<InvestmentProposalDTO[]> {

    constructor(
        private readonly investmentProposalService: InvestmentProposalService,
        private readonly router: Router,
        private readonly notificator: NotificationService,
    ) { }

    resolve() {
        return this.investmentProposalService.getAll()
            .pipe(
                map((proposals: InvestmentProposalDTO[]) => {
                    if (proposals) {
                        return proposals;
                    } else {
                        // this.router.navigate(['/home']);
                        this.notificator.error(`An unexpected error occured.`);
                        return;
                    }
                })
            );
    }
}
