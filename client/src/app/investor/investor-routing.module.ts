import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { AllInvestmentProposalsComponent } from './investment-proposal/all-investment-proposals/all-investment-proposals.component';
import { AllOpenDebtRequestsComponent } from './open-debt-requests/all-open-debt-requests/all-open-debt-requests.component';
import { OpenDebtRequestResolverService } from './open-debt-requests-resolver.service';
import { InvestmentProposalResolverService } from './investor-proposal-resolver.service';

const investorRoutes: Routes = [
  {
    path: '',
    component: AllInvestmentProposalsComponent,
    canActivate: [AuthGuard],
    resolve: { proposals: InvestmentProposalResolverService },
  },
  {
    path: 'debtrequests',
    component: AllOpenDebtRequestsComponent,
    canActivate: [AuthGuard],
    resolve: { openDebtRequests: OpenDebtRequestResolverService }
  },
];

@NgModule({
  imports: [RouterModule.forChild(investorRoutes)],
  exports: [RouterModule]
})
export class InvestorRouterModule { }
