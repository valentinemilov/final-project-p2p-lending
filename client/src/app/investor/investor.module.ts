import { NgModule } from '@angular/core';
import { AllInvestmentProposalsComponent } from './investment-proposal/all-investment-proposals/all-investment-proposals.component';
import { CreateInvestmentProposalComponent } from './open-debt-requests/create-investment-proposal/create-investment-proposal.component';
import { SharedModule } from '../shared/shared.module';
import { InvestorRouterModule } from './investor-routing.module';
import { AllOpenDebtRequestsComponent } from './open-debt-requests/all-open-debt-requests/all-open-debt-requests.component';
import {
  IndividualOpenDebtRequestsComponent
} from './open-debt-requests/individual-open-debt-requests/individual-open-debt-requests.component';
import {
  InvestmentProposalDetailsComponent
} from './investment-proposal/investment-proposal-details/investment-proposal-details.component';
import { OpenDebtRequestResolverService } from './open-debt-requests-resolver.service';
import { InvestmentProposalResolverService } from './investor-proposal-resolver.service';

@NgModule({
  declarations: [
    AllInvestmentProposalsComponent,
    InvestmentProposalDetailsComponent,
    CreateInvestmentProposalComponent,
    AllOpenDebtRequestsComponent,
    IndividualOpenDebtRequestsComponent,
  ],
  providers: [
    OpenDebtRequestResolverService,
    InvestmentProposalResolverService,
  ],
  imports: [
    SharedModule,
    InvestorRouterModule
  ]
})
export class InvestorModule { }
