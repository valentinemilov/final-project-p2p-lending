import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../core/services/notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InvestmentProposalService } from '../../../core/services/investment-proposal.service';
import { CreateInvestmentProposalDTO } from '../../investment-proposal/models/create-investment-proposal.dto';
import { OpenDebtRequestsDTO } from '../../investment-proposal/models/open-debt-requests.dto';
import { UserService } from '../../../core/services/user.service';
import { UserDTO } from '../../../users/models/user.dto';

@Component({
  selector: 'app-all-open-debt-requests',
  templateUrl: './all-open-debt-requests.component.html',
  styleUrls: ['./all-open-debt-requests.component.css']
})
export class AllOpenDebtRequestsComponent implements OnInit {
  userBalance: number;
  public debtRequests: OpenDebtRequestsDTO[] = [];

  constructor(
    private readonly investmentProposalService: InvestmentProposalService,
    private readonly userService: UserService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
  ) { }


  ngOnInit() {
    this.userService
      .getUser()
      .subscribe((data: UserDTO) => {
        (this.userBalance = data.balance);
      });

    this.route.data.subscribe(
      ({ openDebtRequests }) => {
        this.debtRequests = openDebtRequests;
      });
  }

  public createInvestmentProposal(newProposal: CreateInvestmentProposalDTO) {
    if (newProposal.proposed_amount > this.userBalance) {
      this.notificator.error(`You do not have sufficient funds!`);

      return;
    }

    this.investmentProposalService.create(newProposal, newProposal.debtId)

      .subscribe(
        () => {
          this.notificator.success(`Investment proposal successfully created!`);
          this.router.navigate(['/investor']);
        },
        () => this.notificator.error(`Invalid input information!`),
      );
  }
}
