import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { of, throwError } from 'rxjs';
import { AllOpenDebtRequestsComponent } from './all-open-debt-requests.component';
import { SharedModule } from '../../../shared/shared.module';
import { DebtRequestService } from '../../../core/services/debt-request.service';
import { InvestmentProposalService } from '../../../core/services/investment-proposal.service';
import { NotificationService } from '../../../core/services/notification.service';
import { AllInvestmentProposalsComponent } from '../../investment-proposal/all-investment-proposals/all-investment-proposals.component';
import {
    InvestmentProposalDetailsComponent
} from '../../investment-proposal/investment-proposal-details/investment-proposal-details.component';
import { CreateInvestmentProposalComponent } from '../create-investment-proposal/create-investment-proposal.component';
import { IndividualOpenDebtRequestsComponent } from '../individual-open-debt-requests/individual-open-debt-requests.component';
import { CreateInvestmentProposalDTO } from '../../investment-proposal/models/create-investment-proposal.dto';
import { InvesmentProposalStatus } from '../../../shared/enums/invesment-proposal-status.enum';
import { UserService } from '../../../core/services/user.service';

describe('AllOpenDebtRequestsComponent', () => {
    let investmentProposalService;
    let notificator;
    let router;
    let activatedRoute;
    let userService;

    let component: AllOpenDebtRequestsComponent;
    let fixture: ComponentFixture<AllOpenDebtRequestsComponent>;

    const routes: Routes = [
        { path: '', redirectTo: 'investor/debtrequests', pathMatch: 'full' },
        { path: 'investor/debtrequests', component: AllOpenDebtRequestsComponent },
    ];

    beforeEach(async () => {
        jest.clearAllMocks();

        investmentProposalService = {
            create() { }
        };

        notificator = {
            success() { },
            error() { }
        };

        router = {
            navigate() { }
        };

        userService = {
            getUser() { }
        };

        activatedRoute = {
            data: of({
                debtRequests: [],
            })
        };

        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
            ],
            declarations: [
                AllInvestmentProposalsComponent,
                InvestmentProposalDetailsComponent,
                CreateInvestmentProposalComponent,
                AllOpenDebtRequestsComponent,
                IndividualOpenDebtRequestsComponent,
            ],
            providers: [DebtRequestService, InvestmentProposalService, NotificationService]
        })
            .overrideProvider(UserService, { useValue: userService })
            .overrideProvider(InvestmentProposalService, { useValue: investmentProposalService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .overrideProvider(Router, { useValue: router })
            .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
            .compileComponents();
        fixture = TestBed.createComponent(AllOpenDebtRequestsComponent);
        component = fixture.componentInstance;

    });

    it('should be defined', () => {

        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('investmentProposalService.create()', () => {
        it('should call the investmentProposalService.create() once with correct parameters', () => {

            // Arrange
            const mockedProposalData: CreateInvestmentProposalDTO = {
                debtId: '1',
                proposed_amount: 1000,
                proposed_period_months: 12,
                proposed_interest: 0.03,
                creation_date: '2019-11-19',
                penalty_rate: 0.03,
            };

            component.debtRequests = [];
            const mockedReturnedData = 'data';

            const successSpy = jest.spyOn(notificator, 'success');
            const navigateSpy = jest.spyOn(router, 'navigate');

            const spy = jest
                .spyOn(investmentProposalService, 'create')
                .mockReturnValue(of(mockedReturnedData));

            // Act
            component.createInvestmentProposal(mockedProposalData);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(mockedProposalData, mockedProposalData.debtId);
        });

        it(`should subscribe to the investmentProposalService.create() observable and call
        notificationService.success() once when the response is successful`, () => {

                // Arrange
                const mockedProposalData: CreateInvestmentProposalDTO = {
                    debtId: '1',
                    proposed_amount: 1000,
                    proposed_period_months: 12,
                    proposed_interest: 0.03,
                    creation_date: '2019-11-19',
                    penalty_rate: 0.03,
                };

                component.debtRequests = [];
                const mockedReturnedData = 'data';

                // const successSpy = jest.spyOn(notificator, 'success');
                const navigateSpy = jest.spyOn(router, 'navigate');

                const spy = jest
                    .spyOn(investmentProposalService, 'create')
                    .mockReturnValue(of(mockedReturnedData));

                const successSpy = jest.spyOn(notificator, 'success');

                // Act
                component.createInvestmentProposal(mockedProposalData);

                // Assert
                expect(notificator.success).toBeCalledTimes(1);
                expect(notificator.success).toBeCalledWith('Investment proposal successfully created!');
            });

        it('should navigate to the selected investor\'s pending proposals', () => {

            const mockedProposalData: CreateInvestmentProposalDTO = {
                debtId: '1',
                proposed_amount: 1000,
                proposed_period_months: 12,
                proposed_interest: 0.03,
                creation_date: '2019-11-19',
                penalty_rate: 0.03,
            };

            component.debtRequests = [];
            const mockedReturnedData = 'data';

            // Act
            const spyCreate = jest
                .spyOn(investmentProposalService, 'create')
                .mockReturnValue(of(mockedReturnedData));
            const spy = jest.spyOn(router, 'navigate');

            component.createInvestmentProposal(mockedProposalData);

            // Assert
            expect(router.navigate).toHaveBeenCalledWith(['/investor']);
        });

        it(`should subscribe to the investmentProposalService.create() observable and call
        notificationService.error() once when the response is unsuccessful`, () => {
                // Arrange
                const mockedProposalData: CreateInvestmentProposalDTO = {
                    debtId: '1',
                    proposed_amount: 1000,
                    proposed_period_months: 12,
                    proposed_interest: 0.03,
                    creation_date: '2019-11-19',
                    penalty_rate: 0.03,
                };
                component.debtRequests = [];
                const createProposalSpy = jest
                    .spyOn(investmentProposalService, 'create')
                    .mockReturnValue(throwError('Error!'));

                const errorSpy = jest.spyOn(notificator, 'error');

                // Act
                component.createInvestmentProposal(mockedProposalData);

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid input information!');
            });
    });
});
