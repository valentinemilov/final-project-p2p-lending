import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DebtRequestDTO } from '../../../debt-request/models/debt-request-dto';
import { CreateInvestmentProposalDTO } from '../../investment-proposal/models/create-investment-proposal.dto';

@Component({
  selector: 'app-individual-open-debt-requests',
  templateUrl: './individual-open-debt-requests.component.html',
  styleUrls: ['./individual-open-debt-requests.component.css']
})
export class IndividualOpenDebtRequestsComponent {

  @Input() public debtRequest: DebtRequestDTO;
  @Output() public readonly createProposal: EventEmitter<
    CreateInvestmentProposalDTO
  > = new EventEmitter();

  public createProposalStatus = false;

  constructor() { }

  public showCreateProposal = () => {
    if (this.createProposalStatus === false) {
      this.createProposalStatus = true;
    } else {
      this.createProposalStatus = false;
    }
  }

  public createInvestmentProposal(createdProposalForm: CreateInvestmentProposalDTO): void {
    this.createProposal.emit(createdProposalForm);
  }
}
