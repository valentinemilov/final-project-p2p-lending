import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DebtRequestDTO } from '../../../debt-request/models/debt-request-dto';
import { CreateInvestmentProposalDTO } from '../../investment-proposal/models/create-investment-proposal.dto';
import * as moment from 'moment';

@Component({
  selector: 'app-create-investment-proposal',
  templateUrl: './create-investment-proposal.component.html',
  styleUrls: ['./create-investment-proposal.component.css']
})
export class CreateInvestmentProposalComponent implements OnInit {
  @Input() public debtRequest: DebtRequestDTO;
  @Output() public readonly createProposalForm: EventEmitter<
    CreateInvestmentProposalDTO
  > = new EventEmitter();
  public investmentProposalForm: FormGroup;


  constructor(
    private readonly fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.investmentProposalForm = this.fb.group({
      proposed_interest: ['', Validators.compose([Validators.required, Validators.min(0), Validators.pattern('^[0-9]+\.?[0-9]*$')])],
      penalty_rate: ['', Validators.compose([Validators.required, Validators.min(0), Validators.pattern('^[0-9]+\.?[0-9]*$')])],
    });
  }

  public createInvestmentProposal(): void {

    const createdProposalForm = this.investmentProposalForm.value;
    const newProposal: CreateInvestmentProposalDTO = {
      debtId: this.debtRequest.id,
      proposed_amount: +this.debtRequest.target_amount,
      proposed_period_months: +this.debtRequest.target_period_months,
      proposed_interest: +createdProposalForm.proposed_interest / 100,
      creation_date: moment(new Date()).format('YYYY-MM-DD'),
      penalty_rate: +createdProposalForm.penalty_rate / 100,
    };

    this.createProposalForm.emit(newProposal);
  }
}
