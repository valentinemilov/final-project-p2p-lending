import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { DebtRequestService } from '../core/services/debt-request.service';
import { NotificationService } from '../core/services/notification.service';
import { map } from 'rxjs/operators';
import { OpenDebtRequestsDTO } from './investment-proposal/models/open-debt-requests.dto';

@Injectable()
export class OpenDebtRequestResolverService implements Resolve<OpenDebtRequestsDTO[]> {

    constructor(
        private readonly router: Router,
        private readonly debtRequestService: DebtRequestService,
        private readonly notificator: NotificationService,
    ) { }

    resolve() {
        return this.debtRequestService.getAllOpenDebtRequests()
            .pipe(
                map((openDebtRequests: OpenDebtRequestsDTO[]) => {
                    if (openDebtRequests) {
                        return openDebtRequests;
                    } else {
                        // this.router.navigate(['/home']);
                        this.notificator.error(`An unexpected error occured.`);
                        return;
                    }
                })
            );
    }
}
