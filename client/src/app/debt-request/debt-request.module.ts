import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllDebtRequestsComponent } from './all-debt-requests/all-debt-requests.component';
import { DebtRequestRoutingModule } from './debt-request-routing.module';
import { CrateDebtRequestComponent } from './crate-debt-request/crate-debt-request.component';
import { SharedModule } from '../shared/shared.module';
import { DebtRequestDetailsComponent } from './debt-request-details/debt-request-details.component';
import { DebtRequestProposalsComponent } from './debt-request-proposals/debt-request-proposals.component';
import { DebtRequestResolverService } from './all-debt-request-resolver.service';

@NgModule({
  declarations: [
    AllDebtRequestsComponent,
    CrateDebtRequestComponent,
    DebtRequestDetailsComponent,
    DebtRequestProposalsComponent,
  ],
  providers: [
    DebtRequestResolverService,
  ],
  imports: [
    CommonModule,
    DebtRequestRoutingModule,
    SharedModule,
  ]
})
export class DebtRequestModule { }
