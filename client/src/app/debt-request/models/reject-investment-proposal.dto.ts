import { InvesmentProposalStatus } from '../../shared/enums/invesment-proposal-status.enum';

export class RejectInvestmentProposalDTO {

    id: string;

    proposed_amount: number;

    proposed_period_months: number;

    proposed_interest: number;

    proposal_status: InvesmentProposalStatus;

    penalty_rate: number;
}
