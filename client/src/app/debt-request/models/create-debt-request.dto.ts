export class CreateDebtRequestDTO {

    target_amount: number;

    target_period_months: number;

    date: string;
}
