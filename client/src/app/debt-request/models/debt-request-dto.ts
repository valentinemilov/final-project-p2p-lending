import { UserDTO } from '../../users/models/user.dto';
import { InvestDebtRequestDTO } from './invest-debt-request.dto';

export class DebtRequestDTO {

    id: string;

    target_amount: number;

    target_period_months: number;

    date: string;

    user: UserDTO;

    investment_proposals: InvestDebtRequestDTO[];
}
