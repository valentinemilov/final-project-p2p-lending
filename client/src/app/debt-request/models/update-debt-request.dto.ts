export class UpdateDebtRequestDTO {

    id: string;

    target_amount: number;

    target_period_months: number;
}
