import { InvesmentProposalStatus } from '../../shared/enums/invesment-proposal-status.enum';
import { UserDTO } from '../../users/models/user.dto';

export class InvestDebtRequestDTO {

    id: string;

    proposed_amount: number;

    proposed_period_months: number;

    proposed_interest: number;

    penalty_rate: number;

    proposal_status: InvesmentProposalStatus;

    user: UserDTO;
}
