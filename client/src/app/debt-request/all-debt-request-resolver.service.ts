import { DebtRequestDTO } from './models/debt-request-dto';
import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { DebtRequestService } from '../core/services/debt-request.service';
import { NotificationService } from '../core/services/notification.service';
import { map } from 'rxjs/operators';

@Injectable()
export class DebtRequestResolverService implements Resolve<DebtRequestDTO[]> {

    constructor(
        private readonly router: Router,
        private readonly debtRequestService: DebtRequestService,
        private readonly notificator: NotificationService,
    ) { }

    resolve() {
        return this.debtRequestService.getAllDebtRequests()
            .pipe(
                map((debtRequests: DebtRequestDTO[]) => {
                    if (debtRequests) {
                        return debtRequests;
                    } else {
                        this.notificator.error(`An unexpected error occured.`);
                        return;
                    }
                })
            );
    }
}
