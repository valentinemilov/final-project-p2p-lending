import { DebtRequestService } from './../../core/services/debt-request.service';
import { AllDebtRequestsComponent } from './all-debt-requests.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes, ActivatedRoute, Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module';
import { DebtRequestRoutingModule } from '../debt-request-routing.module';
import { CrateDebtRequestComponent } from '../crate-debt-request/crate-debt-request.component';
import { DebtRequestDetailsComponent } from '../debt-request-details/debt-request-details.component';
import { DebtRequestProposalsComponent } from '../debt-request-proposals/debt-request-proposals.component';
import { InvestmentProposalService } from '../../core/services/investment-proposal.service';
import { NotificationService } from '../../core/services/notification.service';
import { LoanService } from '../../core/services/loan.service';
import { DebtRequestDTO } from '../models/debt-request-dto';
import { InvesmentProposalStatus } from '../../shared/enums/invesment-proposal-status.enum';
import { RejectProposalDTO } from '../models/reject-proposal.dto';
import { RejectInvestmentProposalDTO } from '../models/reject-investment-proposal.dto';
import { AcceptProposalDTO } from '../models/accept-proposal.dto';
import { delay } from 'rxjs/operators';

describe('AllInvestmentProposalsComponent', () => {
    let debtRequestService;
    let investmentProposalService;
    let loanService;
    let notificator;
    let activatedRoute;
    let router;

    let component: AllDebtRequestsComponent;
    let fixture: ComponentFixture<AllDebtRequestsComponent>;

    const routes: Routes = [
        { path: '', component: AllDebtRequestsComponent },
        { path: 'newrequest', component: CrateDebtRequestComponent },
        { path: 'loans/borrower', component: AllDebtRequestsComponent },
    ];

    beforeEach(async () => {
        jest.clearAllMocks();

        debtRequestService = {
            deleteDebtRequest() { },
            updateDebtRequest() { },
        };

        investmentProposalService = {
            rejectInvestmentProposal() { },
        };

        loanService = {
            createNewLoan() { },
        };

        notificator = {
            success() { },
            error() { }
        };

        activatedRoute = {
            data: of({
                investmentProposals: [],
            })
        };

        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
                DebtRequestRoutingModule,
            ],
            declarations: [
                AllDebtRequestsComponent,
                CrateDebtRequestComponent,
                DebtRequestDetailsComponent,
                DebtRequestProposalsComponent,
            ],
            providers: [DebtRequestService, InvestmentProposalService, NotificationService, LoanService]
        })
            .overrideProvider(DebtRequestService, { useValue: debtRequestService })
            .overrideProvider(InvestmentProposalService, { useValue: investmentProposalService })
            .overrideProvider(LoanService, { useValue: loanService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
            .compileComponents();
        fixture = TestBed.createComponent(AllDebtRequestsComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit()', () => {

        it('should initialize correctly with the data passed from the resolver', () => {
            // Arrange
            const debtRequests: DebtRequestDTO[] = [{
                id: '123',
                target_amount: 1200,
                target_period_months: 6,
                date: '2019-12-12,',

                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },

                investment_proposals: [{
                    id: '1234',
                    proposed_amount: 1200,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    penalty_rate: 0.01,
                    proposal_status: InvesmentProposalStatus.Pending,

                    user: {
                        id: '2',
                        username: 'Robin',
                        email: 'robinn@b.com',
                        balance: 2000,
                    },
                }]
            }];
            activatedRoute.data = of({ debtRequests });
            fixture = TestBed.createComponent(AllDebtRequestsComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            expect(component.debtRequests).toEqual(debtRequests);
        });
    });

    describe('deleteDebtRequest()', () => {

        it('should call the debtRequestService.deleteDebtRequest() once with correct parameters', () => {
            // Arrange
            const debtRequest: DebtRequestDTO = {
                id: '123',
                target_amount: 1200,
                target_period_months: 6,
                date: '2019-12-12,',

                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },

                investment_proposals: [{
                    id: '1234',
                    proposed_amount: 1200,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    penalty_rate: 0.01,
                    proposal_status: InvesmentProposalStatus.Pending,

                    user: {
                        id: '2',
                        username: 'Robin',
                        email: 'robinn@b.com',
                        balance: 2000,
                    },
                }]
            };

            const spy = jest
                .spyOn(debtRequestService, 'deleteDebtRequest')
                .mockReturnValue(of(debtRequest));

            const mockedDebtRequestToDelete = { id: '123' };
            component.debtRequests = [mockedDebtRequestToDelete as any];
            const mockedDeleteDebtRequestData = { id: 'fake' };

            // Act
            component.deleteDebtRequest(mockedDeleteDebtRequestData as any);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(mockedDeleteDebtRequestData.id);
        });

        it('should subscribe to the debtRequestService.deleteDebtRequest() observable and delete the emitted value from the array', () => {
            // Arrange
            const debtRequest: DebtRequestDTO = {
                id: '123',
                target_amount: 1200,
                target_period_months: 6,
                date: '2019-12-12,',

                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },

                investment_proposals: [{
                    id: '1234',
                    proposed_amount: 1200,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    penalty_rate: 0.01,
                    proposal_status: InvesmentProposalStatus.Pending,

                    user: {
                        id: '2',
                        username: 'Robin',
                        email: 'robinn@b.com',
                        balance: 2000,
                    },
                }]
            };

            const spy = jest
                .spyOn(debtRequestService, 'deleteDebtRequest')
                .mockReturnValue(of(debtRequest));

            const mockedDebtRequestToDelete = { id: '123' };
            component.debtRequests = [mockedDebtRequestToDelete as any];
            const mockedDeleteDebtRequestData = { id: 'fake' };

            // Act
            component.deleteDebtRequest(mockedDeleteDebtRequestData as any);

            // Assert
            expect(component.debtRequests).toEqual([]);
        });

        it(`should subscribe to the debtRequestService.deleteDebtRequest() observable
         and call notificator.success() once when the response is successful`, () => {
                // Arrange
                const debtRequest: DebtRequestDTO = {
                    id: '123',
                    target_amount: 1200,
                    target_period_months: 6,
                    date: '2019-12-12,',

                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },

                    investment_proposals: [{
                        id: '1234',
                        proposed_amount: 1200,
                        proposed_period_months: 6,
                        proposed_interest: 0.02,
                        penalty_rate: 0.01,
                        proposal_status: InvesmentProposalStatus.Pending,

                        user: {
                            id: '2',
                            username: 'Robin',
                            email: 'robinn@b.com',
                            balance: 2000,
                        },
                    }]
                };

                const mockedDebtRequestToDelete = { id: '123' };
                component.debtRequests = [mockedDebtRequestToDelete as any];
                const mockedDeleteDebtRequestData = { id: 'fake' };

                const spy = jest
                    .spyOn(debtRequestService, 'deleteDebtRequest')
                    .mockReturnValue(of(debtRequest));

                const successSpy = jest.spyOn(notificator, 'success');

                // Act
                component.deleteDebtRequest(mockedDeleteDebtRequestData as any);

                // Assert
                expect(notificator.success).toBeCalledTimes(1);
                expect(notificator.success).toBeCalledWith('Debt request successfully deleted!');
            });

        it(`should subscribe to the debtRequestService.deleteDebtRequest() observable
         and call notificator.error() once when the response is unsuccessful`, () => {
                // Arrange
                const debtRequest: DebtRequestDTO = {
                    id: '123',
                    target_amount: 1200,
                    target_period_months: 6,
                    date: '2019-12-12,',

                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },

                    investment_proposals: [{
                        id: '1234',
                        proposed_amount: 1200,
                        proposed_period_months: 6,
                        proposed_interest: 0.02,
                        penalty_rate: 0.01,
                        proposal_status: InvesmentProposalStatus.Pending,

                        user: {
                            id: '2',
                            username: 'Robin',
                            email: 'robinn@b.com',
                            balance: 2000,
                        },
                    }]
                };

                const mockedDebtRequestToDelete = { id: '123' };
                component.debtRequests = [mockedDebtRequestToDelete as any];
                const mockedDeleteDebtRequestData = { id: 'fake' };

                const spy = jest
                    .spyOn(debtRequestService, 'deleteDebtRequest')
                    .mockReturnValue(throwError('Error'));

                const errorSpy = jest.spyOn(notificator, 'error');

                // Act
                component.deleteDebtRequest(mockedDeleteDebtRequestData as any);

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid request!');
            });
    });

    describe('updateDebtRequest()', () => {

        it('should call the debtRequestService.updateDebtRequest() once with correct parameters', () => {
            // Arrange
            const debtRequest: DebtRequestDTO = {
                id: '123',
                target_amount: 1200,
                target_period_months: 6,
                date: '2019-12-12,',

                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },

                investment_proposals: [{
                    id: '1234',
                    proposed_amount: 1200,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    penalty_rate: 0.01,
                    proposal_status: InvesmentProposalStatus.Pending,

                    user: {
                        id: '2',
                        username: 'Robin',
                        email: 'robinn@b.com',
                        balance: 2000,
                    },
                }]
            };

            const spy = jest
                .spyOn(debtRequestService, 'updateDebtRequest')
                .mockReturnValue(of(debtRequest));

            const mockedDebtRequestToUpdate = { id: '123' };
            component.debtRequests = [mockedDebtRequestToUpdate as any];
            const mockedUpdateDebtRequestData = { id: 'fake' };

            // Act
            component.updateDebtRequest(mockedUpdateDebtRequestData as any);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(mockedUpdateDebtRequestData);
        });

        it('should subscribe to debtRequestService.updateDebtRequest() observable and save the emitted value from the array', () => {
            // Arrange
            const debtRequest: DebtRequestDTO = {
                id: '123',
                target_amount: 1200,
                target_period_months: 6,
                date: '2019-12-12,',

                user: {
                    id: '1',
                    username: 'Batman',
                    email: 'batman@b.com',
                    balance: 1000,
                },

                investment_proposals: [{
                    id: '1234',
                    proposed_amount: 1200,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    penalty_rate: 0.01,
                    proposal_status: InvesmentProposalStatus.Pending,

                    user: {
                        id: '2',
                        username: 'Robin',
                        email: 'robinn@b.com',
                        balance: 2000,
                    },
                }]
            };

            const spy = jest
                .spyOn(debtRequestService, 'updateDebtRequest')
                .mockReturnValue(of(debtRequest));

            const mockedDebtRequestToUpdate = { id: '123' };
            component.debtRequests = [mockedDebtRequestToUpdate as any];
            const mockedUpdateDebtRequestData = { id: 'fake' };

            // Act
            component.updateDebtRequest(mockedUpdateDebtRequestData as any);

            // Assert
            expect(component.debtRequests).toEqual([debtRequest]);
        });

        it(`should subscribe to the debtRequestService.updateDebtRequest() observable
         and call notificator.success() once when the response is successful`, () => {
                // Arrange
                const debtRequest: DebtRequestDTO = {
                    id: '123',
                    target_amount: 1200,
                    target_period_months: 6,
                    date: '2019-12-12,',

                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },

                    investment_proposals: [{
                        id: '1234',
                        proposed_amount: 1200,
                        proposed_period_months: 6,
                        proposed_interest: 0.02,
                        penalty_rate: 0.01,
                        proposal_status: InvesmentProposalStatus.Pending,

                        user: {
                            id: '2',
                            username: 'Robin',
                            email: 'robinn@b.com',
                            balance: 2000,
                        },
                    }]
                };

                const mockedDebtRequestToUpdate = { id: '123' };
                component.debtRequests = [mockedDebtRequestToUpdate as any];
                const mockedUpdateDebtRequestData = { id: 'fake' };

                const spy = jest
                    .spyOn(debtRequestService, 'updateDebtRequest')
                    .mockReturnValue(of(debtRequest));

                const successSpy = jest.spyOn(notificator, 'success');

                // Act
                component.updateDebtRequest(mockedUpdateDebtRequestData as any);

                // Assert
                expect(successSpy).toBeCalledTimes(1);
                expect(successSpy).toBeCalledWith('Debt request successfully updated!');
            });

        it(`should subscribe to the debtRequestService.updateDebtRequest() observable
         and call notificator.error() once when the response is unsuccessful`, () => {
                // Arrange
                const debtRequest: DebtRequestDTO = {
                    id: '123',
                    target_amount: 1200,
                    target_period_months: 6,
                    date: '2019-12-12,',

                    user: {
                        id: '1',
                        username: 'Batman',
                        email: 'batman@b.com',
                        balance: 1000,
                    },

                    investment_proposals: [{
                        id: '1234',
                        proposed_amount: 1200,
                        proposed_period_months: 6,
                        proposed_interest: 0.02,
                        penalty_rate: 0.01,
                        proposal_status: InvesmentProposalStatus.Pending,

                        user: {
                            id: '2',
                            username: 'Robin',
                            email: 'robinn@b.com',
                            balance: 2000,
                        },
                    }]
                };

                const mockedDebtRequestToUpdate = { id: '123' };
                component.debtRequests = [mockedDebtRequestToUpdate as any];
                const mockedUpdateDebtRequestData = { id: 'fake' };

                const spy = jest
                    .spyOn(debtRequestService, 'updateDebtRequest')
                    .mockReturnValue(throwError('Error'));

                const errorSpy = jest.spyOn(notificator, 'error');

                // Act
                component.updateDebtRequest(mockedUpdateDebtRequestData as any);

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid request!');
            });
    });

    describe('rejectProposal()', () => {

        it('should call the investmentProposalService.rejectInvestmentProposal() once with correct parameters', () => {
            // Arrange
            const mockRejectProposal: RejectProposalDTO = {
                debtId: '12',
                proposalId: '123',
            };

            const mockData: RejectInvestmentProposalDTO = {
                id: '123',
                proposed_amount: 1000,
                proposed_period_months: 6,
                proposed_interest: 0.02,
                proposal_status: InvesmentProposalStatus.Pending,
                penalty_rate: 0.01,
            };

            const spy = jest
                .spyOn(investmentProposalService, 'rejectInvestmentProposal')
                .mockReturnValue(of(mockData));

            // Act
            component.rejectProposal(mockRejectProposal);

            // Assert
            expect(spy).toBeCalledTimes(1);
            expect(spy).toBeCalledWith(mockRejectProposal.debtId, mockRejectProposal.proposalId);
        });

        it('should subscribe to investmentProposalService.rejectInvestmentProposal() observable and save the emitted value', () => {
            // Arrange
            const mockRejectProposal: RejectProposalDTO = {
                debtId: '12',
                proposalId: '123',
            };

            const mockData: RejectInvestmentProposalDTO = {
                id: '123',
                proposed_amount: 1000,
                proposed_period_months: 6,
                proposed_interest: 0.02,
                proposal_status: InvesmentProposalStatus.Rejected,
                penalty_rate: 0.01,
            };

            const spy = jest
                .spyOn(investmentProposalService, 'rejectInvestmentProposal')
                .mockReturnValue(of(mockData));

            // Act
            component.rejectProposal(mockRejectProposal);

            // Assert
            expect(mockData.proposal_status).toBe(InvesmentProposalStatus.Rejected);
        });

        it(`should subscribe to the investmentProposalService.rejectInvestmentProposal() observable
        and call notificator.success() once when the response is successful`, (done) => {
                // Arrange
                const mockRejectProposal: RejectProposalDTO = {
                    debtId: '12',
                    proposalId: '123',
                };

                const mockData: RejectInvestmentProposalDTO = {
                    id: '123',
                    proposed_amount: 1000,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    proposal_status: InvesmentProposalStatus.Pending,
                    penalty_rate: 0.01,
                };

                const spy = jest
                    .spyOn(investmentProposalService, 'rejectInvestmentProposal')
                    .mockImplementation(() => of(mockData));

                const successSpy = jest.spyOn(notificator, 'success');

                // Act
                component.rejectProposal(mockRejectProposal);
                component.debtRequests = [];

                // Assert
                investmentProposalService.rejectInvestmentProposal().pipe(delay(300)).subscribe(() => {
                    expect(successSpy).toHaveBeenCalledTimes(1);
                    expect(successSpy).toHaveBeenCalledWith('Investment proposal successfully rejected!');

                    done();
                });

            });

        it(`should subscribe to the investmentProposalService.rejectInvestmentProposal() observable
        and call notificator.error() once when the response is unsuccessful`, () => {
                // Arrange
                const mockRejectProposal: RejectProposalDTO = {
                    debtId: '12',
                    proposalId: '123',
                };

                const mockData: RejectInvestmentProposalDTO = {
                    id: '123',
                    proposed_amount: 1000,
                    proposed_period_months: 6,
                    proposed_interest: 0.02,
                    proposal_status: InvesmentProposalStatus.Rejected,
                    penalty_rate: 0.01,
                };

                const spy = jest
                    .spyOn(investmentProposalService, 'rejectInvestmentProposal')
                    .mockReturnValue(throwError('Error'));

                const errorSpy = jest.spyOn(notificator, 'error');

                // Act
                component.rejectProposal(mockRejectProposal);

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid operation!');
            });
    });

    describe('acceptProposal()', () => {

        it(`should subscribe to loanService.createNewLoan() observable
        and call notificator.success() once when the response is successful`, () => {
                // Arrange
                const mockAcceptProposal: AcceptProposalDTO = {
                    debtId: '12',
                    proposalId: '123',
                };

                const spy = jest
                    .spyOn(loanService, 'createNewLoan')
                    .mockReturnValue(of(true));

                const successSpy = jest.spyOn(notificator, 'success');
                const navigateSpy = jest.spyOn(router, 'navigate');

                // Act
                component.acceptProposal(mockAcceptProposal);
                component.debtRequests = [];

                // Assert
                expect(successSpy).toHaveBeenCalledTimes(1);
                expect(successSpy).toHaveBeenCalledWith('New loan created!');
            });

        it(`should call router.navigate() once with correct message when new loan is successfully created`, () => {
                // Arrange
                const mockAcceptProposal: AcceptProposalDTO = {
                    debtId: '12',
                    proposalId: '123',
                };

                const spy = jest
                    .spyOn(loanService, 'createNewLoan')
                    .mockReturnValue(of(true));

                const successSpy = jest.spyOn(notificator, 'success');
                const navigateSpy = jest.spyOn(router, 'navigate');

                // Act
                component.acceptProposal(mockAcceptProposal);
                component.debtRequests = [];

                // Assert
                expect(navigateSpy).toHaveBeenCalledTimes(1);
                expect(navigateSpy).toHaveBeenCalledWith(['/loans/borrower']);
            });

        it(`should subscribe to the loanService.createNewLoan() observable
        and call notificator.error() once when the response is unsuccessful`, () => {
                // Arrange
                const mockAcceptProposal: AcceptProposalDTO = {
                    debtId: '12',
                    proposalId: '123',
                };

                const spy = jest
                    .spyOn(loanService, 'createNewLoan')
                    .mockReturnValue(throwError('Error'));

                const errorSpy = jest.spyOn(notificator, 'error');
                const navigateSpy = jest.spyOn(router, 'navigate');
                // Act
                component.acceptProposal(mockAcceptProposal);
                component.debtRequests = [];

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith('Invalid request!');
            });
    });
});
