import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoanService } from './../../core/services/loan.service';
import { AcceptProposalDTO } from './../models/accept-proposal.dto';
import { InvestmentProposalService } from './../../core/services/investment-proposal.service';
import { DebtRequestService } from './../../core/services/debt-request.service';
import { NotificationService } from '../../core/services/notification.service';
import { DebtRequestDTO } from '../models/debt-request-dto';
import { RejectProposalDTO } from '../models/reject-proposal.dto';
import { InvestmentProposalDTO } from '../../investor/investment-proposal/models/investment-proposal.dto';
import { RejectInvestmentProposalDTO } from '../models/reject-investment-proposal.dto';
import { InvesmentProposalStatus } from '../../shared/enums/invesment-proposal-status.enum';

@Component({
  selector: 'app-all-debt-requests',
  templateUrl: './all-debt-requests.component.html',
  styleUrls: ['./all-debt-requests.component.css']
})
export class AllDebtRequestsComponent implements OnInit {
  public debtRequests: DebtRequestDTO[] = [];
  responsive = true;
  cols = 1;

  constructor(
    private readonly debtRequestService: DebtRequestService,
    private readonly investmentProposalService: InvestmentProposalService,
    private readonly loanService: LoanService,
    private readonly notificator: NotificationService,
    private readonly route: ActivatedRoute,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      ({ debtRequests }) => {
        this.debtRequests = debtRequests;
      });
  }

  public deleteDebtRequest(debtRequest: DebtRequestDTO): void {
    this.debtRequestService
      .deleteDebtRequest(debtRequest.id)
      .subscribe(
        (data: DebtRequestDTO) => {
          this.debtRequests = this.debtRequests.filter(
            (currentDebt: DebtRequestDTO) => currentDebt.id !== data.id),
            this.notificator.success(`Debt request successfully deleted!`);
        },
        () => this.notificator.error(`Invalid request!`),
      );
  }

  public updateDebtRequest(debtRequest: DebtRequestDTO): void {
    this.debtRequestService
      .updateDebtRequest(debtRequest)
      .subscribe((data: DebtRequestDTO) => {
        const index: number = this.debtRequests.findIndex(
          (current: DebtRequestDTO) => current.id === data.id
        );
        this.debtRequests[index] = data;
        this.notificator.success(`Debt request successfully updated!`);
      },
        () => this.notificator.error(`Invalid request!`),
      );
  }

  public rejectProposal(rejectProposalOnDebtRequest: RejectProposalDTO): void {
    const debtRequestId: string = rejectProposalOnDebtRequest.debtId;
    const investmentProposalId: string = rejectProposalOnDebtRequest.proposalId;

    this.investmentProposalService
      .rejectInvestmentProposal(debtRequestId, investmentProposalId)
      .subscribe((data: RejectInvestmentProposalDTO) => {

        const currentDebtrequest = this.debtRequests
          .find((curr: DebtRequestDTO) => curr.id === debtRequestId);

        if (currentDebtrequest) {
          const proposal = currentDebtrequest.investment_proposals
            .find((prop: InvestmentProposalDTO) => prop.id === investmentProposalId);

          if (proposal) {
            proposal.proposal_status = data.proposal_status;
          }
        }

        this.notificator.success(`Investment proposal successfully rejected!`);
      },
        () => this.notificator.error(`Invalid operation!`));
  }

  public acceptProposal(acceptProposalOnDebtRequest: AcceptProposalDTO): void {
    const debtRequestId: string = acceptProposalOnDebtRequest.debtId;
    const investmentProposalId: string = acceptProposalOnDebtRequest.proposalId;

    const currentDebtrequest = this.debtRequests
      .find((curr: DebtRequestDTO) => curr.id === debtRequestId);

    if (currentDebtrequest) {
      const proposal = currentDebtrequest.investment_proposals
        .find((prop: InvestmentProposalDTO) => prop.id === investmentProposalId);

      if (proposal) {
        proposal.proposal_status = InvesmentProposalStatus.Accepted;
      }
    }

    this.loanService
      .createNewLoan(debtRequestId, investmentProposalId)
      .subscribe(
        () => {
          this.router.navigate(['/loans/borrower']);
          this.notificator.success('New loan created!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }
}
