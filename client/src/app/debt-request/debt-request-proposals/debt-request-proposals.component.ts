import { Component, Input, Output, EventEmitter } from '@angular/core';
import { InvestDebtRequestDTO } from '../models/invest-debt-request.dto';

@Component({
  selector: 'app-debt-request-proposals',
  templateUrl: './debt-request-proposals.component.html',
  styleUrls: ['./debt-request-proposals.component.css']
})
export class DebtRequestProposalsComponent {
  @Input() public investmentProposal: InvestDebtRequestDTO;

  @Output() public rejectInvestmentProposal: EventEmitter<InvestDebtRequestDTO> = new EventEmitter();
  @Output() public acceptInvestmentProposal: EventEmitter<InvestDebtRequestDTO> = new EventEmitter();

  constructor() { }

  public onAcceptProposal(): void {
    this.acceptInvestmentProposal.emit(this.investmentProposal);
  }

  public onRejectProposal(): void {
    this.rejectInvestmentProposal.emit(this.investmentProposal);
  }
}
