import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { CreateDebtRequestDTO } from './../models/create-debt-request.dto';
import { DebtRequestService } from '../../core/services/debt-request.service';
import { NotificationService } from '../../core/services/notification.service';
import { DebtRequestDTO } from '../models/debt-request-dto';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crate-debt-request',
  templateUrl: './crate-debt-request.component.html',
  styleUrls: ['./crate-debt-request.component.css']
})
export class CrateDebtRequestComponent implements OnInit {
  public createDebtRequestForm: FormGroup;
  public isPartial = false;

  constructor(
    private readonly fb: FormBuilder,
    private readonly debtRequestService: DebtRequestService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.createDebtRequestForm = this.fb.group({
      target_amount: ['', Validators.compose([Validators.required, Validators.min(1), Validators.pattern('^[0-9]+\.?[0-9]*$')])],
      target_period_months: ['', Validators.compose([Validators.required, Validators.min(1), Validators.pattern('^[0-9]+\.?[0-9]*$')])],
    });
  }

  public createDebtRequest(): void {
    const debtRequestformValues: CreateDebtRequestDTO = this.createDebtRequestForm.value;
    const debtRequestToCreate: CreateDebtRequestDTO = {
      target_amount: +debtRequestformValues.target_amount,
      target_period_months: +debtRequestformValues.target_period_months,
      date: moment(new Date()).format('YYYY-MM-DD')
    };

    this.debtRequestService
      .createDebtRequest(debtRequestToCreate)
      .subscribe(
        (data: DebtRequestDTO) => {
          data.investment_proposals = [];
          this.notificator.success(`Debt request successfully created!`);
          this.router.navigate(['/borrower']);
        },
        () => this.notificator.error(`Invalid information!`),
      );
  }
}
