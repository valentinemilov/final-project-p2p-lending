import { CrateDebtRequestComponent } from './crate-debt-request.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '../../shared/shared.module';
import { DebtRequestRoutingModule } from '../debt-request-routing.module';
import { AllDebtRequestsComponent } from '../all-debt-requests/all-debt-requests.component';
import { DebtRequestDetailsComponent } from '../debt-request-details/debt-request-details.component';
import { DebtRequestProposalsComponent } from '../debt-request-proposals/debt-request-proposals.component';
import { DebtRequestService } from '../../core/services/debt-request.service';
import { NotificationService } from '../../core/services/notification.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { CreateDebtRequestDTO } from '../models/create-debt-request.dto';
import * as moment from 'moment';

describe('CreateDebtRequestComponent', () => {
    let debtRequestService;
    let notificator;
    let router;
    let formBuilder;

    let component: CrateDebtRequestComponent;
    let fixture: ComponentFixture<CrateDebtRequestComponent>;

    const routes: Routes = [
        { path: '', component: AllDebtRequestsComponent },
        { path: 'newrequest', component: CrateDebtRequestComponent },
        { path: 'borrower', component: CrateDebtRequestComponent },
    ];

    beforeEach(async () => {
        jest.clearAllMocks();

        debtRequestService = {
            createDebtRequest() { },
        };

        notificator = {
            success() { },
            error() { }
        };

        formBuilder = {
            group() { }
        };

        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule.withRoutes(routes),
                SharedModule,
                DebtRequestRoutingModule,
            ],
            declarations: [
                AllDebtRequestsComponent,
                CrateDebtRequestComponent,
                DebtRequestDetailsComponent,
                DebtRequestProposalsComponent,
            ],
            providers: [DebtRequestService, NotificationService]
        })
            .overrideProvider(DebtRequestService, { useValue: debtRequestService })
            .overrideProvider(NotificationService, { useValue: notificator })
            .overrideProvider(FormBuilder, { useValue: formBuilder })
            .compileComponents();
        fixture = TestBed.createComponent(CrateDebtRequestComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
        component.createDebtRequestForm = new FormGroup({
            target_amount: new FormControl(),
            target_period_months: new FormControl(),
        });

        jest.mock('moment', () => () => ({ format: () => '2019–12-10' }));
    });

    it('should be defined', () => {
        // Arrange & Act & Assert
        expect(component).toBeDefined();
    });

    describe('ngOnInit()', () => {

        it('should set the createDebtRequestForm filled value', () => {
            // Arrange
            component.createDebtRequestForm.controls.target_amount.setValue('10');
            component.createDebtRequestForm.controls.target_period_months.setValue('1');

            const mockData = {
                target_amount: '10',
                target_period_months: '1',
            };

            const spy = jest.spyOn(formBuilder, 'group').mockReturnValue(mockData);

            // Act
            component.ngOnInit();

            // Assert
            expect(component.createDebtRequestForm).toEqual(mockData);
        });

        it('call debtRequestService.createDebtRequest() once with correct parameters', () => {
            // Arrange
            const mockDebtRequest: CreateDebtRequestDTO = {
                target_amount: 1000,
                target_period_months: 10,
                date: '2019-12-10',
            };

            component.createDebtRequestForm.controls.target_amount.setValue('1000');
            component.createDebtRequestForm.controls.target_period_months.setValue('10');

            const spy = jest.spyOn(debtRequestService, 'createDebtRequest').mockReturnValue(of('string'));
            const dateSpy = jest.spyOn(moment(), 'format').mockImplementation(() => '2019-12-10');
            const navigateSpy = jest.spyOn(router, 'navigate');

            // Act
            component.createDebtRequest();

            // Assert
            expect(spy).toBeCalledTimes(1);
        });

        it(`should subscribe to the debtRequestService.createDebtRequest() observable
        and call notificator.success() once when the response is successful`, () => {
                // Arrange
                const mockDebtRequest: CreateDebtRequestDTO = {
                    target_amount: 1000,
                    target_period_months: 10,
                    date: '2019-12-10',
                };

                component.createDebtRequestForm.controls.target_amount.setValue('1000');
                component.createDebtRequestForm.controls.target_period_months.setValue('10');

                const spy = jest.spyOn(debtRequestService, 'createDebtRequest').mockReturnValue(of(mockDebtRequest));
                const dateSpy = jest.spyOn(moment(), 'format').mockImplementation(() => '2019-12-10');
                const successSpy = jest.spyOn(notificator, 'success');
                const navigateSpy = jest.spyOn(router, 'navigate');

                // Act
                component.createDebtRequest();

                // Assert
                expect(successSpy).toBeCalledTimes(1);
                expect(successSpy).toBeCalledWith(`Debt request successfully created!`);
            });

        it(`should call router.navigate() once with correct message when debt request is successfully created`, () => {
            // Arrange
            const mockDebtRequest: CreateDebtRequestDTO = {
                target_amount: 1000,
                target_period_months: 10,
                date: '2019-12-10',
            };

            component.createDebtRequestForm.controls.target_amount.setValue('1000');
            component.createDebtRequestForm.controls.target_period_months.setValue('10');

            const spy = jest.spyOn(debtRequestService, 'createDebtRequest').mockReturnValue(of(mockDebtRequest));
            const dateSpy = jest.spyOn(moment(), 'format').mockImplementation(() => '2019-12-10');
            const successSpy = jest.spyOn(notificator, 'success');
            const navigateSpy = jest.spyOn(router, 'navigate');

            // Act
            component.createDebtRequest();

            // Assert
            expect(navigateSpy).toBeCalledTimes(1);
            expect(navigateSpy).toBeCalledWith(['/borrower']);
        });

        it(`should subscribe to the debtRequestService.createDebtRequest() observable
        and call notificator.error() once when the response is unsuccessful`, () => {
                // Arrange
                const mockDebtRequest: CreateDebtRequestDTO = {
                    target_amount: 1000,
                    target_period_months: 10,
                    date: '2019-12-10',
                };

                component.createDebtRequestForm.controls.target_amount.setValue('1000');
                component.createDebtRequestForm.controls.target_period_months.setValue('10');

                const spy = jest.spyOn(debtRequestService, 'createDebtRequest').mockReturnValue(throwError('Error'));
                const dateSpy = jest
                    .spyOn(moment(), 'format')
                    .mockImplementation(() => '2019-12-10');

                const errorSpy = jest.spyOn(notificator, 'error');
                const navigateSpy = jest.spyOn(router, 'navigate');

                // Act
                component.createDebtRequest();

                // Assert
                expect(errorSpy).toBeCalledTimes(1);
                expect(errorSpy).toBeCalledWith(`Invalid information!`);
            });
    });
});
