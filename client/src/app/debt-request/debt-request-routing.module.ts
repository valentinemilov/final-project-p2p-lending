import { AuthGuard } from './../core/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllDebtRequestsComponent } from './all-debt-requests/all-debt-requests.component';
import { DebtRequestResolverService } from './all-debt-request-resolver.service';
import { CrateDebtRequestComponent } from './crate-debt-request/crate-debt-request.component';

const debtRequestRoutes: Routes = [
  {
    path: '',
    component: AllDebtRequestsComponent,
    canActivate: [AuthGuard],
    resolve: { debtRequests: DebtRequestResolverService }
  },
  {
    path: 'newrequest',
    component: CrateDebtRequestComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(debtRequestRoutes)],
  exports: [RouterModule]
})
export class DebtRequestRoutingModule { }
