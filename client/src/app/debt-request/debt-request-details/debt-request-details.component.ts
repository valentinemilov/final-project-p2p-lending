import { Component, Input, EventEmitter, Output } from '@angular/core';
import { DebtRequestDTO } from './../models/debt-request-dto';
import { InvestDebtRequestDTO } from '../models/invest-debt-request.dto';
import { RejectProposalDTO } from '../models/reject-proposal.dto';
import { AcceptProposalDTO } from '../models/accept-proposal.dto';

@Component({
  selector: 'app-debt-request-details',
  templateUrl: './debt-request-details.component.html',
  styleUrls: ['./debt-request-details.component.css']
})
export class DebtRequestDetailsComponent {
  @Input() public debtRequest: DebtRequestDTO;

  @Output() public deleteDebtRequest: EventEmitter<DebtRequestDTO> = new EventEmitter();
  @Output() public updateDebtRequest: EventEmitter<DebtRequestDTO> = new EventEmitter();
  @Output() public rejectProposal: EventEmitter<RejectProposalDTO> = new EventEmitter();
  @Output() public acceptProposal: EventEmitter<AcceptProposalDTO> = new EventEmitter();

  constructor() { }

  public onDeleteDebtRequest(): void {
    this.deleteDebtRequest.emit(this.debtRequest);
  }

  public updateAmount(amount: number): void {
    this.debtRequest.target_amount = +amount;
  }

  public updatePeriod(period: number): void {
    this.debtRequest.target_period_months = +period;
  }

  public onEditDebtRequest(): void {
    this.updateDebtRequest.emit(this.debtRequest);
  }

  public rejectInvestmentProposal(proposal: InvestDebtRequestDTO): void {
    const debtRequestId: string = this.debtRequest.id;
    const rejectProposalOnDebtRequest: RejectProposalDTO = {
      debtId: debtRequestId,
      proposalId: proposal.id,
    };

    this.rejectProposal.emit(rejectProposalOnDebtRequest);
  }

  public acceptInvestmentProposal(proposal: InvestDebtRequestDTO): void {
    const debtRequestId: string = this.debtRequest.id;
    const acceptProposalOnDebtRequest: AcceptProposalDTO = {
      debtId: debtRequestId,
      proposalId: proposal.id,
    };

    this.acceptProposal.emit(acceptProposalOnDebtRequest);
  }

}
