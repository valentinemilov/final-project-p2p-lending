import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { WithdrawDTO } from '../models/withdraw.dto';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {

  @Output() public newWithdraw: EventEmitter<WithdrawDTO> = new EventEmitter();

  public withdrawForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.withdrawForm = this.fb.group({
      withdraw: ['', [Validators.required, Validators.min(1)]],
    });
  }

  public withdrawFunds(): void {
    const withdraw = this.withdrawForm.value;
    const newWithdraw: WithdrawDTO = {
      withdraw: +withdraw.withdraw
    };

    this.newWithdraw.emit(newWithdraw);
  }
}
