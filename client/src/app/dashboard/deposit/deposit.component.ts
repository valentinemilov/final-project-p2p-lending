import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DepositDTO } from '../models/deposit.dto';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  @Output() public newDeposit: EventEmitter<DepositDTO> = new EventEmitter();

  public depositForm: FormGroup;

  constructor(
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.depositForm = this.fb.group({
      deposit: ['', [Validators.required, Validators.min(1)]],
    });
  }

  public depositFunds(): void {
    const deposit = this.depositForm.value;
    const newDeposit: DepositDTO = {
      deposit: +deposit.deposit
    };

    this.newDeposit.emit(newDeposit);
  }
}
