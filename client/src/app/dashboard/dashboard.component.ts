import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NotificationService } from '../core/services/notification.service';
import { UserService } from '../core/services/user.service';
import { DepositDTO } from './models/deposit.dto';
import { UserDTO } from '../users/models/user.dto';
import { LoanService } from '../core/services/loan.service';
import { LoanDTO } from '../loans/models/loans.dto';
import { WithdrawDTO } from './models/withdraw.dto';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  depositStatus = false;
  withdrawStatus = false;

  public userBalance: number;
  public investorLoans: number;
  public totalInvestments: number;
  public borrowerLoans: number;
  public totalDebts: number;

  constructor(
    private readonly userService: UserService,
    private readonly loanService: LoanService,
    private readonly notificator: NotificationService,
    private readonly fb: FormBuilder,
  ) { }

  public ngOnInit() {

    this.userService
      .getUser()
      .subscribe((data: UserDTO) => {
        (this.userBalance = data.balance);
      });

    this.loanService
      .getAllInvestorLoans()
      .subscribe((data: LoanDTO[]) => {
        const total: number = data.reduce((a, b) => a + b.amount, 0);
        this.totalInvestments = total;
        this.investorLoans = data.length;
      });

    this.loanService
      .getAllBorrowerLoans()
      .subscribe((data: LoanDTO[]) => {
        const total: number = data.reduce((a, b) => a + b.amount, 0);
        this.totalDebts = total;
        this.borrowerLoans = data.length;
      });
  }

  public showDepositField = () => {
    if (this.depositStatus === false) {
      this.depositStatus = true;
    } else {
      this.depositStatus = false;
    }
  }

  public showWithdrawField = () => {
    if (this.withdrawStatus === false) {
      this.withdrawStatus = true;
    } else {
      this.withdrawStatus = false;
    }
  }

  public depositFunds(deposit: DepositDTO): void {

    this.userService
      .deposit(deposit)
      .subscribe(
        () => {
          this.userBalance = this.userBalance + Number(deposit.deposit);
          this.depositStatus = false;
          this.notificator.success('Deposit was successful!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }

  public withdrawFunds(withdraw: WithdrawDTO): void {

    if (this.userBalance < withdraw.withdraw) {
      return this.notificator.error('Cannot withdraw above balance!');
    }

    this.userService
      .withdraw(withdraw)
      .subscribe(
        () => {
          this.userBalance = this.userBalance - withdraw.withdraw;
          this.withdrawStatus = false;
          this.notificator.success('Withdraw was successful!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }

}
