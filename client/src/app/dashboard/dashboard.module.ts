import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardRouterModule } from './dashboard-routing.module';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { DepositComponent } from './deposit/deposit.component';

@NgModule({
  declarations: [
    DashboardComponent,
    WithdrawComponent,
    DepositComponent,
  ],
  imports: [
    SharedModule,
    DashboardRouterModule
  ]
})
export class DashboardModule {

}
