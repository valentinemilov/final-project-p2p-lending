import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
  ) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['', ([Validators.required, Validators.minLength(4), Validators.maxLength(20)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    });
  }

  public register() {

    this.authService.register(this.registerForm.value)
      .subscribe(
        () => {
          this.notificator.success(`Registration successful!`);

          this.router.navigate(['users/login']);
        },
        () => this.notificator.error(`There was an error processing the request!`),
      );
  }
}
