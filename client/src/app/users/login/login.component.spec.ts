import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router, Routes } from '@angular/router';

describe('LoginComponent', () => {
  let authService;
  let notificationService;
  let router;
  let formBuilder;

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const routes: Routes = [
    { path: '', redirectTo: 'users/login', pathMatch: 'full' },
    { path: 'users/login', component: LoginComponent },
  ];

  beforeEach(async () => {
    jest.clearAllMocks();

    authService = {
      login() { }
    };

    notificationService = {
      success() { },
      error() { }
    };

    router = {
      navigate() { }
    };

    formBuilder = {
      group() { }
    };

    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), ReactiveFormsModule, SharedModule],
      declarations: [LoginComponent],
      providers: [AuthService, NotificationService]
    })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(NotificationService, { useValue: notificationService })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .compileComponents();
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(component).toBeDefined();
  });

  describe('login()', () => {
    it('should set the loginForm filed value', () => {

      // Arrange
      const mockedLoginForm = 'login form';

      const loginFormConfigurations = {
        username: 'Asdasad',
        password: 'asd123'
      };

      const spy = jest
        .spyOn(formBuilder, 'group')
        .mockReturnValue(mockedLoginForm);

      // Act
      component.ngOnInit();

      // Assert
      expect(component.loginForm).toEqual(mockedLoginForm);
    });

  });
});
