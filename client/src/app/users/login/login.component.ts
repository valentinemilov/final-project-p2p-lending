import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { NotificationService } from '../../core/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificationService,
    private readonly router: Router,
    private readonly fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public login() {
    this.authService.login(this.loginForm.value)
      .subscribe(
        () => {
          this.notificator.success(`Login successful!`);
          this.router.navigate(['/dashboard']);
        },
        () => this.notificator.error(`Invalid username/password!`),
      );
  }
}
