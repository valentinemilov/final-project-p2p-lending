import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { CreatePaymentsDTO } from '../models/create-payment.dto';
import { LoanStatus } from '../../shared/enums/loan-status.enum';
import { LoanCalculationsService } from '../../core/services/loan-calculations.service';
import { LoanDTO } from '../models/loans.dto';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-borrower-loan-table',
  templateUrl: './borrower-loan-table.component.html',
  styleUrls: ['./borrower-loan-table.component.css']
})
export class BorrowerLoanTableComponent implements OnInit {

  @Input() public loan: LoanDTO;
  @Output() public readonly createPayment: EventEmitter<
    CreatePaymentsDTO
  > = new EventEmitter();

  monthlyLoanInstallment: number;
  monthlyInterestAmount: number;
  currentMonthOverdueAmount: number;
  monthlyTotalAmount: number;
  currentInstallmentDueDate: string;

  dateToday: string;
  daysOverdue: number;
  amountOverdue: number;
  installmentsOverdue: number;

  loanStatus: LoanStatus;

  constructor(
    private readonly loanCalculationService: LoanCalculationsService,
  ) {
    this.dataSource = new MatTableDataSource([this.loan]);
  }


  displayedColumns = ['Investor', 'Amount', 'Interest', 'Amount Overdue', 'Total', 'Due Date', 'Status', 'Days Overdue'];
  dataSource: MatTableDataSource<LoanDTO>;


  ngOnInit() {
    const totalLoanInterestAmount: number = this.loanCalculationService.totalLoanInterestAmount(this.loan);

    const monthlyLoanInstallment: number = this.loanCalculationService.monthlyLoanInstallment(this.loan);
    this.monthlyLoanInstallment = monthlyLoanInstallment;

    const monthlyInterestAmount: number = this.loanCalculationService.monthlyInterestAmount(this.loan, totalLoanInterestAmount);
    this.monthlyInterestAmount = monthlyInterestAmount;

    const dateToday: string = this.loanCalculationService.dateToday();
    this.dateToday = dateToday;

    const currentInstallmentDueDate: string = this.loanCalculationService.currentInstallmentDueDate(this.loan);
    this.currentInstallmentDueDate = currentInstallmentDueDate;

    const daysOverdue: number = this.loanCalculationService.overdueDays(dateToday, currentInstallmentDueDate);
    this.daysOverdue = daysOverdue;

    const loanStatusCheck: LoanStatus = this.loanCalculationService.loanStatusCheck(daysOverdue);
    this.loanStatus = loanStatusCheck;

    const amountOverdue: number = this.loanCalculationService.amountOverdue(this.loan, daysOverdue, monthlyLoanInstallment);
    this.amountOverdue = amountOverdue;

    const monthlyTotalAmount: number =
      this.loanCalculationService.monthlyTotalAmount(monthlyLoanInstallment, monthlyInterestAmount, amountOverdue);
    this.monthlyTotalAmount = monthlyTotalAmount;
  }

  public createPaymentMethod(): void {
    const newPayment: CreatePaymentsDTO = {

      loanId: this.loan.id,

      amount_paid: this.monthlyLoanInstallment,

      interest_paid: this.monthlyInterestAmount,

      overdue_paid: this.amountOverdue,

      payment_date: this.dateToday,
    };

    this.createPayment.emit(newPayment);
  }
}
