import { BorrowerLoansResolverService } from './borrower-loans-resolver.service';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoanRouterModule } from './loans-routing.module';
import { BorrowerAllLoansComponent } from './borrower-all-loans/borrower-all-loans.component';
import { InvestorLoanTableComponent } from './investor-loan-table/investor-loan-table.component';
import { StartToDateLoanComponent } from './start-to-date-loan-table.component/start-to-date-loan-table.component';
import { TotalLoanTableComponent } from './total-loan-table.component/total-loan-table.component';
import { InvestorAllLoansComponent } from './investor-all-loans/investor-all-loans.component';
import { BorrowerLoanTableComponent } from './borrower-loan-table/borrower-loan-table.component';
import { InvestorLoansResolverService } from './investor-loans-resolver.service';

@NgModule({
  declarations: [
    InvestorAllLoansComponent,
    BorrowerAllLoansComponent,
    InvestorLoanTableComponent,
    TotalLoanTableComponent,
    StartToDateLoanComponent,
    BorrowerLoanTableComponent,
  ],
  providers: [
    InvestorLoansResolverService,
    BorrowerLoansResolverService,
  ],
  imports: [
    SharedModule,
    LoanRouterModule
  ],
})
export class LoansModule { }
