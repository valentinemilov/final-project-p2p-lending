import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../core/guards/auth.guard';
import { BorrowerAllLoansComponent } from './borrower-all-loans/borrower-all-loans.component';
import { InvestorLoansResolverService } from './investor-loans-resolver.service';
import { BorrowerLoansResolverService } from './borrower-loans-resolver.service';
import { InvestorAllLoansComponent } from './investor-all-loans/investor-all-loans.component';

const loanRoutes: Routes = [
  {
    path: 'investor', component: InvestorAllLoansComponent,
    canActivate: [AuthGuard],
    resolve: { investorLoans: InvestorLoansResolverService }
  },
  {
    path: 'borrower',
    component: BorrowerAllLoansComponent,
    canActivate: [AuthGuard],
    resolve: { borrowerLoans: BorrowerLoansResolverService }
  },
];

@NgModule({
  imports: [RouterModule.forChild(loanRoutes)],
  exports: [RouterModule]
})

export class LoanRouterModule { }
