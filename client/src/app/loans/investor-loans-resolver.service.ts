import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { NotificationService } from '../core/services/notification.service';
import { map } from 'rxjs/operators';
import { LoanService } from '../core/services/loan.service';
import { LoanDTO } from './models/loans.dto';

@Injectable()
export class InvestorLoansResolverService implements Resolve<LoanDTO[]> {

    constructor(
        private readonly loanService: LoanService,
        private readonly notificator: NotificationService,
    ) { }

    resolve() {
        return this.loanService.getAllInvestorLoans()
            .pipe(
                map((investorLoans: LoanDTO[]) => {
                    if (investorLoans) {
                        return investorLoans;
                    } else {
                        this.notificator.error(`An unexpected error occured.`);
                        return;
                    }
                })
            );
    }
}
