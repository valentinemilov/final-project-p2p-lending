import { Component, OnInit } from '@angular/core';
import { LoanDTO } from '../models/loans.dto';
import { NotificationService } from '../../core/services/notification.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-investor-all-loans',
  templateUrl: './investor-all-loans.component.html',
  styleUrls: ['./investor-all-loans.component.css']
})
export class InvestorAllLoansComponent implements OnInit {

  public loans: LoanDTO[] = [];
  responsive = true;
  cols = 1;

  constructor(
    private readonly notificator: NotificationService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      ({ investorLoans }) => {
        this.loans = investorLoans;
      });
  }
}
