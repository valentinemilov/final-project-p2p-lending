import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { NotificationService } from '../core/services/notification.service';
import { map } from 'rxjs/operators';
import { LoanService } from '../core/services/loan.service';
import { LoanDTO } from './models/loans.dto';

@Injectable()
export class BorrowerLoansResolverService implements Resolve<LoanDTO[]> {

    constructor(
        private readonly loanService: LoanService,
        private readonly notificator: NotificationService,
    ) { }

    resolve() {
        return this.loanService.getAllBorrowerLoans()
            .pipe(
                map((borrowerLoans: LoanDTO[]) => {
                    if (borrowerLoans) {
                        return borrowerLoans;
                    } else {
                        this.notificator.error(`An unexpected error occured.`);
                        return;
                    }
                })
            );
    }
}
