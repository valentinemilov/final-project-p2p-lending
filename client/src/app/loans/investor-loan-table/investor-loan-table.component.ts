
import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { LoanCalculationsService } from '../../core/services/loan-calculations.service';
import { LoanStatus } from '../../shared/enums/loan-status.enum';
import { LoanDTO } from '../models/loans.dto';

@Component({
  selector: 'app-investor-loan-table',
  templateUrl: './investor-loan-table.component.html',
  styleUrls: ['./investor-loan-table.component.css']
})
export class InvestorLoanTableComponent implements OnInit {

  @Input() public loan: LoanDTO;

  monthlyLoanInstallment: number;
  monthlyInterestAmount: number;
  currentMonthOverdueAmount: number;
  monthlyTotalAmount: number;
  currentInstallmentDueDate: string;

  dateToday: string;
  daysOverdue: number;
  amountOverdue: number;
  installmentsOverdue: number;

  loanStatus: LoanStatus;

  constructor(
    private readonly loanCalculationService: LoanCalculationsService,
  ) {
    this.dataSource = new MatTableDataSource([this.loan]);
  }

  displayedColumns = ['Borrower', 'Amount', 'Interest', 'Amount Overdue', 'Total', 'Due Date', 'Status', 'Days Overdue'];
  dataSource: MatTableDataSource<LoanDTO>;

  ngOnInit() {
    const totalLoanInterestAmount: number = this.loanCalculationService.totalLoanInterestAmount(this.loan);

    const monthlyLoanInstallment: number = this.loanCalculationService.monthlyLoanInstallment(this.loan);
    this.monthlyLoanInstallment = monthlyLoanInstallment;

    const monthlyInterestAmount: number = this.loanCalculationService.monthlyInterestAmount(this.loan, totalLoanInterestAmount);
    this.monthlyInterestAmount = monthlyInterestAmount;

    const dateToday: string = this.loanCalculationService.dateToday();
    this.dateToday = dateToday;

    const currentInstallmentDueDate: string = this.loanCalculationService.currentInstallmentDueDate(this.loan);
    this.currentInstallmentDueDate = currentInstallmentDueDate;

    const daysOverdue: number = this.loanCalculationService.overdueDays(dateToday, currentInstallmentDueDate);
    this.daysOverdue = daysOverdue;

    const installmentsOverdue: number = this.loanCalculationService.installmentsOverdue(dateToday, currentInstallmentDueDate);
    this.installmentsOverdue = installmentsOverdue;

    const loanStatusCheck: LoanStatus = this.loanCalculationService.loanStatusCheck(daysOverdue);
    this.loanStatus = loanStatusCheck;

    const amountOverdue: number = this.loanCalculationService.amountOverdue(this.loan, daysOverdue, monthlyLoanInstallment);
    this.amountOverdue = amountOverdue;

    const monthlyTotalAmount: number =
      this.loanCalculationService.monthlyTotalAmount(monthlyLoanInstallment, monthlyInterestAmount, amountOverdue);
    this.monthlyTotalAmount = monthlyTotalAmount;
  }
}
