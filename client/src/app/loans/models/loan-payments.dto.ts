
export class LoanPaymentsDTO {

    id: string;

    amount_paid: number;

    interest_paid: number;

    overdue_paid: number;

    payment_date: string;
}
