import { UserDTO } from '../../users/models/user.dto';
import { LoanPaymentsDTO } from './loan-payments.dto';

export class LoanDTO {

    id: string;

    amount: number;

    period_months: number;

    annual_interest: number;

    creation_date: string;

    penalty_rate: number;

    isClosed: boolean;

    isDeleted: boolean;

    investor: UserDTO;

    borrower: UserDTO;

    payments: LoanPaymentsDTO[];
}
