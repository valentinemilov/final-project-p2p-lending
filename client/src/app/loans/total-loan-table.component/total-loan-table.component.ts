import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { LoanDTO } from '../models/loans.dto';
import { LoanCalculationsService } from '../../core/services/loan-calculations.service';

@Component({
  selector: 'app-total-loan-table',
  templateUrl: './total-loan-table.component.html',
  styleUrls: ['./total-loan-table.component.css']
})
export class TotalLoanTableComponent implements OnInit {

  @Input() public loan: LoanDTO;

  monthlyLoanInstallment: number;
  monthlyInterestAmount: number;
  totalLoanInterestAmount: number;

  constructor(
    private readonly loanCalculationService: LoanCalculationsService,
  ) {
    this.dataSource = new MatTableDataSource([this.loan]);
  }

  displayedColumns = [
    'Installments Total',
    'Loan Amount Total',
    'Interest Amount Total',
    'Total Amount Total',
    'Annual Interest',
    'Creation Date',
    'Borrower',
    'Investor',
  ];
  dataSource: MatTableDataSource<LoanDTO>;

  ngOnInit() {

    const totalLoanInterestAmount: number = this.loanCalculationService.totalLoanInterestAmount(this.loan);
    this.totalLoanInterestAmount = totalLoanInterestAmount;

    const monthlyLoanInstallment: number = this.loanCalculationService.monthlyLoanInstallment(this.loan);
    this.monthlyLoanInstallment = monthlyLoanInstallment;

    const monthlyInterestAmount: number = this.loanCalculationService.monthlyInterestAmount(this.loan, totalLoanInterestAmount);
    this.monthlyInterestAmount = monthlyInterestAmount;
  }
}
