import { Component, OnInit } from '@angular/core';
import { LoanDTO } from '../models/loans.dto';
import { NotificationService } from '../../core/services/notification.service';
import { CreatePaymentsDTO } from '../models/create-payment.dto';
import { PaymentService } from '../../core/services/payments.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-borrower-all-loans',
  templateUrl: './borrower-all-loans.component.html',
  styleUrls: ['./borrower-all-loans.component.css']
})
export class BorrowerAllLoansComponent implements OnInit {

  public loans: LoanDTO[] = [];

  constructor(
    private readonly paymentsService: PaymentService,
    private readonly notificator: NotificationService,
    private readonly route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.route.data.subscribe(
      ({ borrowerLoans }) => {
        this.loans = borrowerLoans;
      });
  }

  public createPayment(newPayment: CreatePaymentsDTO): void {

    this.paymentsService
      .create(newPayment)
      .subscribe(
        () => {
          this.notificator.success('Payment successful!');
        },
        () => {
          this.notificator.error('Invalid request!');
        }
      );
  }
}
