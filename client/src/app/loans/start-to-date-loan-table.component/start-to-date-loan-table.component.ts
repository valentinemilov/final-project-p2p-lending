import { Component, OnInit, Input } from '@angular/core';
import { LoanStatus } from '../../shared/enums/loan-status.enum';
import { MatTableDataSource } from '@angular/material';
import { LoanCalculationsService } from '../../core/services/loan-calculations.service';
import { LoanDTO } from '../models/loans.dto';

@Component({
  selector: 'app-start-to-date-loan-table',
  templateUrl: './start-to-date-loan-table.component.html',
  styleUrls: ['./start-to-date-loan-table.component.css']
})
export class StartToDateLoanComponent implements OnInit {

  @Input() public loan: LoanDTO;

  monthlyLoanInstallment: number;
  monthlyInterestAmount: number;
  currentMonthOverdueAmount: number;
  monthlyTotalAmount: number;
  currentInstallmentDueDate: string;

  installmentsPaid: number;
  installmentsLeft: number;

  loanAmountPaid: number;
  loanAmountLeft: number;
  interestAmountPaid: number;
  interestAmountLeft: number;
  totalLoanInterestAmount: number;
  totalAmountPaid: number;
  totalAmountLeft: number;

  dateToday: string;
  daysOverdue: number;
  amountOverdue: number;
  installmentsOverdue: number;

  loanStatus: LoanStatus;

  constructor(
    private readonly loanCalculationService: LoanCalculationsService,
  ) {
    this.dataSource = new MatTableDataSource([this.loan]);
  }

  displayedColumns = [
    'Installments Paid',
    'Loan Amount Paid',
    'Interest Amount Paid',
    'Total Amount Paid',
    'Installments Left',
    'Loan Amount Left',
    'Interest Amount Left',
    'Installments Overdue',
  ];
  dataSource: MatTableDataSource<LoanDTO>;

  ngOnInit() {

    const totalLoanInterestAmount: number = this.loanCalculationService.totalLoanInterestAmount(this.loan);
    this.totalLoanInterestAmount = totalLoanInterestAmount;

    const monthlyInterestAmount: number = this.loanCalculationService.monthlyInterestAmount(this.loan, totalLoanInterestAmount);
    this.monthlyInterestAmount = monthlyInterestAmount;

    const installmentsPaid: number = this.loanCalculationService.installmentsPaid(this.loan);
    this.installmentsPaid = installmentsPaid;

    const installmentsLeft: number = this.loanCalculationService.installmentsLeft(this.loan, installmentsPaid);
    this.installmentsLeft = installmentsLeft;

    const loanAmountPaid: number = this.loanCalculationService.loanAmountPaid(this.loan);
    this.loanAmountPaid = loanAmountPaid;

    const loanAmountLeft: number = this.loanCalculationService.loanAmountLeft(this.loan, loanAmountPaid);
    this.loanAmountLeft = loanAmountLeft;

    const interestAmountPaid: number = this.loanCalculationService.interestAmountPaid(monthlyInterestAmount, installmentsPaid);
    this.interestAmountPaid = interestAmountPaid;

    const interestAmountLeft: number = this.loanCalculationService.interestAmountLeft(totalLoanInterestAmount, interestAmountPaid);
    this.interestAmountLeft = interestAmountLeft;

    const totalAmountPaid: number = this.loanCalculationService.totalAmountPaid(loanAmountPaid, interestAmountPaid);
    this.totalAmountPaid = totalAmountPaid;

    const totalAmountLeft: number = this.loanCalculationService.totalAmountLeft(loanAmountLeft, interestAmountLeft);
    this.totalAmountLeft = totalAmountLeft;

    const dateToday: string = this.loanCalculationService.dateToday();
    this.dateToday = dateToday;

    const currentInstallmentDueDate: string = this.loanCalculationService.currentInstallmentDueDate(this.loan);
    this.currentInstallmentDueDate = currentInstallmentDueDate;

    const installmentsOverdue: number = this.loanCalculationService.installmentsOverdue(dateToday, currentInstallmentDueDate);
    this.installmentsOverdue = installmentsOverdue;
  }
}
