import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-edit-in-place',
  templateUrl: './edit-in-place.component.html',
  styleUrls: ['./edit-in-place.component.css']
})
export class EditInPlaceComponent {

  @Input() prop: number;
  @Output() update = new EventEmitter<any>();

  public updating = false;

  public switchUpdating() {
    this.updating = !this.updating;

    if (!this.updating) {
      this.triggerUpdate(this.prop);
    }
  }

  public triggerUpdate(prop: any) {
    this.update.emit(prop);
  }
}
