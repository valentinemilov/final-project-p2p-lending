import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../users/models/user.dto';
import { AuthService } from '../../../core/services/auth.service';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public loggedUserData: UserDTO;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
  ) { }

  ngOnInit() {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => (this.loggedUserData = data)
    );
  }

  public logout() {
    this.authService.logout();
  }
}
