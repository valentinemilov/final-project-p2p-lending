export enum LoanStatus {

    Overdue = 'Overdue',

    Regular = 'Regular',
}
