import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { EditInPlaceComponent } from './components/edit-in-place/edit-in-place.component';
import { HighlightDirective } from './directives/highlight.directive';
import { FormatAmountPipe } from './pipes/format-amount.pipe';
import { ShowPercentPipe } from './pipes/show-percent.pipe';
import { TransformInvestmentProposalStatus } from './pipes/investment-proposal-status.pipe';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { InvestorNavigationComponent } from './components/investor-navigation/investor-navigation.component';
import { BorrowerNavigationComponent } from './components/borrower-navigation/borrower-navigation.component';
import { ConvertToStringPipe } from './pipes/convert-to-string.pipe';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    EditInPlaceComponent,
    HighlightDirective,
    FormatAmountPipe,
    ShowPercentPipe,
    TransformInvestmentProposalStatus,
    InvestorNavigationComponent,
    BorrowerNavigationComponent,
    ConvertToStringPipe,
    PageNotFoundComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    EditInPlaceComponent,
    HighlightDirective,
    FormatAmountPipe,
    ShowPercentPipe,
    TransformInvestmentProposalStatus,
    FlexLayoutModule,
    InvestorNavigationComponent,
    BorrowerNavigationComponent,
    ConvertToStringPipe,
  ]
})
export class SharedModule { }
