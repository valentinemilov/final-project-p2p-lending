import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'proposalStatus'
})
export class TransformInvestmentProposalStatus implements PipeTransform {

    transform(status: number): any {
        if (status === 1) {
            return 'Pending';
        } else if (status === 2) {
            return 'Accepted';
        }

        return 'Rejected';
    }
}
