import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'showPercent'
})
export class ShowPercentPipe implements PipeTransform {

  transform(percent: number): string {
    if (percent === undefined || percent === null) {

      return;
  }
    return (percent * 100).toFixed(2);
  }
}
