import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'convertToString'
})
export class ConvertToStringPipe implements PipeTransform {

    transform(num: number): string {
        if (num === undefined || num === null) {

            return;
        }

        return num.toFixed(2);
    }
}
