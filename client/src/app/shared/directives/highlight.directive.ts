import {
  Directive,
  ElementRef,
  Renderer2,
  HostListener,
  Input
} from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(
    private readonly el: ElementRef,
    private readonly renderer: Renderer2
  ) { }

  @Input() highlightColor: string;

  @HostListener('mouseenter')
  highlight() {
    this.renderer.setStyle(
      this.el.nativeElement,
      'backgroundColor',
      this.highlightColor
    );
  }

  @HostListener('mouseleave')
  unHighlight() {
    this.renderer.setStyle(this.el.nativeElement, 'backgroundColor', null);
  }

}
